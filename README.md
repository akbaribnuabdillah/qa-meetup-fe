# empfang-frontend

User signup frontend for Kata Platform

## Geting started

Install this starter (assuming you have `gatsby-cli` installed) by running the following command:

```bash
gatsby new project-name https://github.com/resir014/gatsby-starter-typescript-plus
```

## Developing

A nodejs >= 6.0.0 setup with [yarn](https://yarnpkg.com/) is recommended.

```bash
# install dependencies
yarn

# ...or, for npm
npm install

# serve with hot reload at localhost:8000
npm start

# build for production
npm run build

# build for production and push to gh-pages branch
npm run deploy
```

## Environment variables

The environment variables used in this project is available on the `.env.example` file. To use it, copy the file into `.env`. You can also name this file based on your Node environment (e.g. `.env.development` or `.env.production`)

```
## Docker envs https://github.com/gatsbyjs/gatsby-docker
SERVER_NAME=user.katalabs.io
HTTP_PORT=80

## Gatsby build time URLs
GATSBY_EMPFANG_URL=https://user-api.katalabs.io
GATSBY_ZAUN_URL=https://zaun.katalabs.io
GATSBY_WEB_URL=https://kata.ai
GATSBY_PLATFORM_URL=http://platform.kata.ai
GATSBY_RECAPTCHA_SECRET=6Ldx5z8UAAAAAKDtEYz7LzSXs21f30MIYoreLL5u
GATSBY_GOOGLE_GTAG_ID=AW-810115091
GATSBY_GOOGLE_AW_CONVERSION_ID=810115091
GATSBY_GOOGLE_AW_CONVERSION_LABEL=aOeKCLnBuIgBEJPApYID
```

## Credits

Built with [Gatsby](https://www.gatsbyjs.org/) - the blazing-fast static site generator for [React](https://facebook.github.io/react/).
