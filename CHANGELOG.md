# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

- Change environment variables

## [3.2.0] - 2019-05-17

- Updated Kata.ai logo

## [3.1.0] - 2019-02-12

- rewritten in [Gatsby.js](https://www.gatsbyjs.org)
- allow inviting CMS Client users who have not registered

## [3.0.1] - 2018-12-12

- added git revision in meta tags

## [3.0.0] - 2018-12-04

- update to platform 3.0

## [1.1.4] - 2018-10-22

- fix validation form

## [1.1.3] - 2018-09-25

- Add google ads tag to track conversion

## [1.1.2] - 2018-08-13

- Use empfang-backend to login

## [1.1.1] - 2018-05-15

- Show error messages on signup
- Fix admin login

## [1.1.0] - 2018-05-03

- Revamp UI

## [1.0.0] - 2018-04-13

- Move frontend part to new repository
- Add validation to user signup

[unreleased]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/HEAD..v3.2.0#diff
[3.2.0]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v3.2.0..v3.1.0#diff
[3.1.0]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v3.1.0..v3.0.1#diff
[3.0.1]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v3.0.1..v3.0.0#diff
[3.0.0]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v3.0.0..v1.1.4#diff
[1.1.4]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v1.1.4..v1.1.3#diff
[1.1.3]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v1.1.3..v1.1.2#diff
[1.1.2]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v1.1.2..v1.1.1#diff
[1.1.1]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v1.1.1..v1.1.0#diff
[1.1.0]: https://bitbucket.org/yesboss/empfang-frontend/branches/compare/v1.1.0..v1.0.0#diff
