'use strict';

// Implement the Gatsby API "createPages". This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = ({
  actions,
  graphql
}) => {
  // need createRedirect action in actions collection
  // to make the redirection magic happen.
  // https://www.gatsbyjs.org/docs/bound-action-creators/
  const {
    createRedirect
  } = actions;

  // One-off redirect, note trailing slash missing on fromPath and
  // toPath here.
  createRedirect({
    fromPath: `/`,
    isPermanent: true,
    redirectInBrowser: true,
    toPath: `/signup`,
  });
};

// Implement the Gatsby API "onCreatePage". This is
// called after every page is created.
exports.onCreatePage = async ({
  page,
  actions
}) => {
  const {
    createPage
  } = actions;

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/confirm/)) {
    page.matchPath = '/confirm/*';

    // Update the page.
    createPage(page);
  }

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/invite/)) {
    page.matchPath = '/invite/*';

    // Update the page.
    createPage(page);
  }
};
