from .rabbit_message import RabbitMessage
from typing import Dict, Any

class ResampleInputMessage(RabbitMessage):

    def __init__(self, jobId: str, audio_file: str, channel_id: str, config_id: str):
        super().__init__(jobId)
        self.audio_file = audio_file
        self.channel_id = channel_id
        self.config_id = config_id
    
    def to_dict(self):
        data = {
                "id": self.job_id,
                "audioFile": self.audio_file,
                "configId": self.config_id,
                "channelId": self.channel_id
        }
        return data
    
    @staticmethod
    def new_instance(data: Any):
        return ResampleInputMessage(data["id"], data["audioFile"], data["configId"], data["channelId"])