from .rabbit_message import RabbitMessage
from typing import Dict

class ResampleOutputMessage(RabbitMessage):

    def __init__(self, jobId: str, audio_file: str, config_id: str, sample_rate: int, audio_format: str, channel_id: str):
        super().__init__(jobId)
        self.audio_file = audio_file
        self.config_id = config_id
        self.sample_rate = sample_rate
        self.audio_format = audio_format
        self.channel_id = channel_id
    
    def to_dict(self) -> Dict[str, str]:
        data = {
                "id": self.job_id,
                "audioFile": self.audio_file,
                "configId": self.config_id,
                "sampleRate": self.sample_rate,
                "audioFormat": self.audio_format,
                "channelId": self.channel_id
        }
        return data

    @staticmethod
    def new_instance(resample_output: Dict[str, str]):
        data = resample_output
        return ResampleOutputMessage(data["id"], data["audioFile"], data["configId"], 
                data["sampleRate"], data["audioFormat"], data["channelId"])