from abc import ABC, abstractmethod
from typing import Dict, Any

class RabbitMessage(ABC):

    def __init__(self, jobId: str):
        self.job_id = jobId

    @abstractmethod
    def to_dict(self) -> Dict[str, str]:
        pass

    @staticmethod
    @abstractmethod
    def new_instance(data: Dict[str, str]) -> Any:
        pass