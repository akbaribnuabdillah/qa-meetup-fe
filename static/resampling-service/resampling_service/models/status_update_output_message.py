from .rabbit_message import RabbitMessage
from typing import Dict

class StatusUpdateMessage(RabbitMessage):
    def __init__(self, jobId: str, updateTo: str):
        super().__init__(jobId)
        self.update_to = updateTo

    def to_dict(self) -> Dict[str, str]:
        data = {
            "id": self.job_id,
            "status": self.update_to
        }
        return data
    
    @staticmethod
    def new_instance(data: Dict[str, str]) -> None:
        pass