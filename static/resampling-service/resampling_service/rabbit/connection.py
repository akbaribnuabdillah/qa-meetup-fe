from resampling_service.settings import *
import pika
import os

class Connection:
    def __init__(self):
        params: pika.URLParameters = pika.URLParameters(RABBIT_CONNECTION_URL)
        self.connection: pika.BlockingConnection = pika.BlockingConnection(params)
    
    def get_channel(self):
        channel = self.connection.channel()
        return channel