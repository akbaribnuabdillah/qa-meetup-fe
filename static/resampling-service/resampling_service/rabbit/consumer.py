from resampling_service.settings import * 
from resampling_service.rabbit import Connection
import pika
from pika.channel import Channel
from ..tasks import Task

class Consumer:
    def __init__(self, connection: Connection, callback_object: Task):
        self.callback = callback_object
        self.connection = connection
        self.channel = self.connection.get_channel()

    def exchange_consume(self, exchange, routing_key='', exchange_type='direct', queue=''):
        self.channel.exchange_declare(exchange, exchange_type)
        result = self.channel.queue_declare(queue)
        queue_name = result.method.queue
        self.channel.queue_bind(queue=queue_name, exchange=exchange, routing_key=routing_key)
        self.channel.basic_consume(queue_name, on_message_callback=self.base_callback)
        self.channel.start_consuming()
    
    def base_callback(self, ch: Channel, method, properties, body):
        self.callback.run(body)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def queue_consume(self, queue: str, durable: bool):
        self.channel.queue_declare(queue, durable=durable)
        self.channel.basic_consume(queue, on_message_callback=self.base_callback)
        self.channel.start_consuming()