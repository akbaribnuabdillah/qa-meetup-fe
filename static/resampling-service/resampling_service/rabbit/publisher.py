from resampling_service.settings import * 
from resampling_service.rabbit import Connection
import pika
import logging
import json

class Publisher:
    def __init__(self, connection: Connection):
        self.connection = connection
        self.channel = self.connection.get_channel()

    def publish(self, exchange, message, exchange_type='fanout', routing_key=''):
        try:
            self.channel.exchange_declare(exchange=exchange, exchange_type=exchange_type, durable=True)
            self.channel.basic_publish(exchange=exchange, routing_key=routing_key, body=json.dumps(message))
        except Exception as e:
            logging.error(str(e))
            self.connection = Connection()
            self.channel = self.connection.get_channel()
            self.channel.exchange_declare(exchange=exchange, exchange_type=exchange_type, durable=True)
            self.channel.basic_publish(exchange=exchange, routing_key=routing_key, body=json.dumps(message))
    
    def sendToQueue(self, queue: str, message: dict):
        try:
            self.channel.queue_declare(queue=queue, durable=True)
            self.channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(message))
        except Exception as e:
            logging.error(str(e))
            self.connection = Connection()
            self.channel = self.connection.get_channel()
            self.channel.queue_declare(queue=queue, durable=True)
            self.channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(message))