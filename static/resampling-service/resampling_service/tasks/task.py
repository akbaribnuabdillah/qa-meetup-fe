from abc import ABC, abstractmethod

class Task(ABC):    
    @abstractmethod
    def run(self, message_body):
        pass