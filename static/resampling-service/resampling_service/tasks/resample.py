from librosa import resample, load
from soundfile import write, read
import tempfile, os, json
from resampling_service.settings import *
from ..models import *
from .task import Task
import numpy as np
import base64
import logging
import time


class ResampleTask(Task):
    def __init__(self, status_publisher, resample_publisher):
        self.status_publisher = status_publisher
        self.resample_publisher = resample_publisher
    
    def run(self, message_body):
        resample_input = ResampleInputMessage.new_instance(json.loads(message_body.decode('utf-8')))
        audio_bytes = base64.b64decode(resample_input.audio_file)
        self.__status_update__(resample_input.job_id, "processing")
        try:
            # sample_rate = 12000
            audio_format = "FLAC"
            start_time = time.time()
            result, sample_rate = self.__resample_audio__(resample_input.job_id, audio_bytes, None, audio_format)
            elapsed_time = time.time() - start_time

            logging.info("Resampling take {:.2f} seconds".format(elapsed_time))
            output_data = ResampleOutputMessage(resample_input.job_id, result.decode('utf-8'), 
                                        resample_input.config_id, sample_rate, audio_format,
                                        resample_input.channel_id)
            self.resample_publisher.publish(JOB_RESAMPLE_RESULT_EXCHANGE, output_data.to_dict())
        except Exception as e:
            logging.error('Exception occurred in job %s', resample_input.job_id, exc_info=True)
            logging.error(e)
    
    def __resample_audio__(self, jobId, audio_bytes, new_sample_rate, audio_format):
        with tempfile.NamedTemporaryFile(dir=os.path.dirname(__file__)) as audio_file:
            audio_file.write(audio_bytes)
            y, sr = load(audio_file.name, sr=new_sample_rate)
            write(audio_file.name, y, sr, format=audio_format)
            audio_file.seek(0)
            logging.info('Job with ID %s resampled from %dHz to %dHz and converted to %s format', jobId, 
                            sr, sr, audio_format)
            return base64.b64encode(audio_file.read()), sr
    
    def __status_update__(self, jobId, update_to: str):
        data = StatusUpdateMessage(jobId, update_to)
        logging.info('Job with ID %s status updated to %s', jobId, update_to)     
        self.status_publisher.sendToQueue(queue=JOB_STATUS_UPDATE_QUEUE, message=data.to_dict())
