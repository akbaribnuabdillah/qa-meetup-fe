import os

RABBIT_CONNECTION_URL = os.environ.get('RABBIT_CONNECTION_URL', 'amqp://localhost')

# Arrival
JOB_WILL_RESAMPLE_QUEUE = os.environ.get('JOB_WILL_RESAMPLE_QUEUE', 'job.will_resample')

# Departue
JOB_RESAMPLE_RESULT_EXCHANGE = os.environ.get('JOB_RESAMPLE_RESULT_EXCHANGE', 'job.resample_result')
JOB_STATUS_UPDATE_QUEUE = os.environ.get('JOB_STATUS_UPDATE_QUEUE', 'job.status_update')