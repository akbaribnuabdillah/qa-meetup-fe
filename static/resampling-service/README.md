## Development Setup

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt

# Install Dependencies
pipenv install --dev
```
