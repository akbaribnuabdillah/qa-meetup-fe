from resampling_service.rabbit import Connection, Consumer, Publisher
from resampling_service.tasks import ResampleTask
from resampling_service.settings import *
import logging

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)
    consumer_connection = Connection()
    publisher_connection = Connection()

    status_update_publisher = Publisher(publisher_connection)
    resample_publisher = Publisher(publisher_connection)

    resample_consumer = Consumer(consumer_connection, ResampleTask(status_update_publisher, resample_publisher))
    resample_consumer.queue_consume(queue=JOB_WILL_RESAMPLE_QUEUE, durable=True)