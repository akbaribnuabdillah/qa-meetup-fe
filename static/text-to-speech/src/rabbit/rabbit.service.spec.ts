import { Test, TestingModule } from '@nestjs/testing';
import { RabbitService } from './rabbit.service';
import * as amqp from 'amqplib';

jest.mock('amqplib');

describe('RabbitService Test', () => {
    let rabbitService: RabbitService;

    // @ts-ignore
    const mockedChannel: jest.Mocked<amqp.Channel> = {
        sendToQueue: jest.fn(),
        consume: jest.fn(),
        assertQueue: jest.fn(),
        assertExchange: jest.fn(),
        publish: jest.fn(),
        bindQueue: jest.fn(),
    };

    // @ts-ignore
    const mockedConnection: jest.Mocked<amqp.Connection> = {
        createChannel: jest.fn().mockReturnValue(mockedChannel),
    };

    beforeEach(async () => {
        // @ts-ignore
        jest.spyOn(amqp, 'connect').mockImplementation(async () => mockedConnection);

        rabbitService = new RabbitService();
    });

    afterEach(() => {
        mockedChannel.assertQueue.mockClear();
        mockedChannel.assertExchange.mockClear();
    });

    it('should create new channel from connection', async () => {
        const promiseMockedConn = async () => mockedConnection;
        await rabbitService.createChannel(promiseMockedConn());
        expect(mockedConnection.createChannel).toBeCalled();
    });

    describe('channel ops', () => {
        it('sendToQueue should change message to buffer and send it', async () => {
            jest.spyOn(rabbitService, 'mToBuffer').mockReturnValue(Buffer.from(JSON.stringify({ id: 'mock' })));
            await rabbitService.sendToQueue('mock', { id: 'mock' });
            expect(mockedChannel.assertQueue).toHaveBeenCalledWith('mock', { durable: true });
            expect(mockedChannel.sendToQueue).toHaveBeenCalledWith(
                'mock',
                Buffer.from(JSON.stringify({ id: 'mock' })),
                { persistent: true },
            );
        });
        it('publish should change message to buffer and publish it', async () => {
            jest.spyOn(rabbitService, 'mToBuffer').mockReturnValue(Buffer.from(JSON.stringify({ id: 'mock' })));
            await rabbitService.publish('mock', { id: 'mock' });
            expect(mockedChannel.assertExchange).toHaveBeenCalledWith('mock', 'fanout');
            expect(mockedChannel.publish).toHaveBeenCalledWith(
                'mock',
                '',
                Buffer.from(JSON.stringify({ id: 'mock' })),
            );
        });
        it('consume should change buffer to JSON and feed it to callback', async () => {
            jest.spyOn(rabbitService, 'mToBuffer').mockReturnValue(Buffer.from(JSON.stringify({ id: 'mock' })));
            await rabbitService.consume(
                { queue: 'mock1', callback: jest.fn() },
                { queue: 'mock2', callback: jest.fn() },
            );
            expect(mockedChannel.assertQueue).toHaveBeenCalledTimes(2);
            expect(mockedChannel.consume).toHaveBeenCalledTimes(2);
        });
        it('subscribe should change buffer to JSON and feed it to callback', async () => {
            jest.spyOn(rabbitService, 'mToBuffer').mockReturnValue(Buffer.from('mock'));
            await rabbitService.subscribe(
                { exchange: 'mock1', callback: jest.fn() },
                { exchange: 'mock2', callback: jest.fn() },
            );
            expect(mockedChannel.assertExchange).toHaveBeenCalledTimes(2);
            expect(mockedChannel.assertQueue).toHaveBeenCalledTimes(2);
            expect(mockedChannel.consume).toHaveBeenCalledTimes(2);
        });
    });
});
