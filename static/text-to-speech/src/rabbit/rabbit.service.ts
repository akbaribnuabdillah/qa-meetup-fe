import { Injectable, Logger } from '@nestjs/common';
import * as amqp from 'amqplib';
import * as configFile from '../config';

type consumerCallback = (message: any) => any;
type consumerParams = { callback: consumerCallback, queue: string };
type subscriberParams = { callback: consumerCallback, exchange: string };
@Injectable()
export class RabbitService {
    consumerChannel: Promise<amqp.Channel>;
    publisherChannel: Promise<amqp.Channel>;

    constructor() {
        const consumerConnection = amqp.connect(configFile.RABBIT_CONNECTION_URL);
        const publisherConnection = amqp.connect(configFile.RABBIT_CONNECTION_URL);
        this.consumerChannel = this.createChannel(consumerConnection);
        this.publisherChannel = this.createChannel(publisherConnection);
    }

    async createChannel(connection: Promise<amqp.Connection>): Promise<amqp.Channel> {
        const conn = await connection;
        return conn.createChannel();
    }

    async consume(...args: consumerParams[]) {
        if (args.length > 0) {
            const chan = await this.consumerChannel;
            args.forEach(arg => {
                try {
                    const { callback, queue } = arg;
                    chan.assertQueue(queue);
                    chan.consume(queue, msg => {
                        if (msg.content) {
                            const data = this.mFromBufferToJson(msg.content);
                            try {
                                callback(data);
                            } catch (err) {
                                Logger.error(err.message);
                            } finally {
                                chan.ack(msg);
                            }
                        }
                    });
                } catch (err) {
                    Logger.error(err.message);
                }
            });
        }
    }

    async sendToQueue(queue: string, message: any) {
        const bufferMsg = this.mToBuffer(message);
        const chan = await this.publisherChannel;
        chan.assertQueue(queue, { durable: true });
        chan.sendToQueue(queue, bufferMsg, { persistent: true });
    }

    async publish(exchange: string, message: any) {
        const bufferMsg = this.mToBuffer(message);
        const chan = await this.publisherChannel;
        chan.assertExchange(exchange, 'fanout');
        chan.publish(exchange, '', bufferMsg);
    }

    async subscribe(...args: subscriberParams[]) {
        if (args.length > 0) {
            const chan = await this.consumerChannel;
            args.forEach(async arg => {
                try {
                    const { callback, exchange } = arg;
                    chan.assertExchange(exchange, 'fanout');
                    const q = await chan.assertQueue('');
                    chan.bindQueue(q.queue, exchange, '');
                    chan.consume(q.queue, msg => {
                        if (msg.content) {
                            const data = this.mFromBufferToJson(msg.content);
                            try {
                                callback(data);
                            } catch (err) {
                                Logger.error(err.message);
                            } finally {
                                chan.ack(msg);
                            }
                        }
                    });
                } catch (err) {
                    Logger.error(err.message);
                }
            });
        }
    }

    mToBuffer(thing: any) {
        return Buffer.from(JSON.stringify(thing));
    }

    mFromBufferToJson(buff: Buffer) {
        return JSON.parse(buff.toString());
    }
}
