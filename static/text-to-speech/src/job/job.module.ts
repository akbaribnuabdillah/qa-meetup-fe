import { Module } from '@nestjs/common';
import { JobController } from './job.controller';
import { JobService } from './job.service';
import { RabbitService } from '../rabbit/rabbit.service';

@Module({
  controllers: [JobController],
  providers: [JobService, RabbitService],
})
export class JobModule {}
