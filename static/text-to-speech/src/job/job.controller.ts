import { Controller, Post, Res, Body, HttpStatus } from '@nestjs/common';
import { JobService } from './job.service';
import { Response } from 'express'
import { T2SDto } from './job.dto';

@Controller('t2s')
export class JobController {

    constructor(private readonly service: JobService) {}

    @Post()
    async create(@Body() t2sDto: T2SDto, @Res() res: Response) {
       const path =  await this.service.predict(t2sDto);
       res.status(HttpStatus.OK).json(path);
    }

}
