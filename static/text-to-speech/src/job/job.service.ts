import { Injectable, Logger } from '@nestjs/common';
import { T2SDto } from './job.dto';
import * as textToSpeech from '@google-cloud/text-to-speech';
import { RabbitService } from '../rabbit/rabbit.service';
import * as fs from 'fs';
import * as util from 'util';
import * as configFile from '../config';
import { JobDto, IUserConfigDto } from '../dto';
import axios from 'axios';
import { JobResult } from '../interfaces/job';

@Injectable()
export class JobService {

    constructor(
        private readonly rabbitService: RabbitService,
    ) {
        this.rabbitService.consume(
            { queue: configFile.JOB_WILL_PREDICT_T2S_QUEUE, callback: this.predictJob },
        );
    }

    readonly predictJob = async (payload: JobDto) => {
        const { id, message, configId, channelId } = payload;
        const { accountData } = await this.mGetUserConfig(configId);
        const client = new textToSpeech.TextToSpeechClient( {credentials: accountData });
        const request = {
            input: {text: message},
            voice: {languageCode: 'id-ID', name: 'id-ID-Standard-B'},
            audioConfig: {audioEncoding: 'MP3'},
        };

        try {
            const [response] = await client.synthesizeSpeech(request);
        // Write the binary audio content to a local file
            const data: JobResult = {
                id,
                result: response.audioContent.toString('base64'),
                channelId,
                status: 'success',
            };
            this.rabbitService.publish(configFile.JOB_PREDICT_T2S_RESULT_EXCHANGE, data);
        } catch (err) {

            const data: JobResult = {
                id,
                result: null,
                channelId,
                status: 'failed',
            };
            Logger.error(err.message);
            this.rabbitService.sendToQueue(configFile.JOB_PREDICT_T2S_RESULT_EXCHANGE, data);
        }
    }

    public async predict(t2sDto: T2SDto): Promise<any> {
        const {message, config} = t2sDto;

        const client = new textToSpeech.TextToSpeechClient( {credentials: config });
        const request = {
            input: {text: message},
            voice: {languageCode: 'id-ID', name: 'id-ID-Standard-B'},
            audioConfig: {audioEncoding: 'MP3'},
        };

        const [response] = await client.synthesizeSpeech(request);
        // Write the binary audio content to a local file
        const writeFile = util.promisify(fs.writeFile);
        console.log(response);
        await writeFile('output.mp3', Buffer.from(JSON.stringify(response.audioContent)), 'binary');
        return ('Audio content written to file: output.mp3');
    }

    async mGetUserConfig(configId: string) {
        const uri = `${configFile.STORAGE_SERVICE_URL}/project/configs/${configId}`;
        const { data } = await axios.get(uri);
        const userConfig: IUserConfigDto = data as IUserConfigDto;
        return userConfig;
    }
}
