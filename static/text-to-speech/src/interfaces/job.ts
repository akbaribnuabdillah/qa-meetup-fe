export interface JobResult {
    id: string;
    result: Buffer;
    channelId: string;
    status: 'failed' | 'success';
}
