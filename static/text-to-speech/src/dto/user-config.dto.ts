import { IGoogleServiceAccount } from '../interfaces/account';

export interface IUserConfigDto {
    id: string;
    projectId: string;
    accountData: IGoogleServiceAccount;
}
