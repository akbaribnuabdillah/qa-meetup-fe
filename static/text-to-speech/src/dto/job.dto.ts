import { IGoogleServiceAccount } from '../interfaces/account';

export interface JobDto {
    id: string;
    message: string;
    configId: string;
    channelId: string;
}

export interface IUserConfigDto {
    id: string;
    projectId: string;
    provider: 'google';
    accountData: IGoogleServiceAccount;
}
