export const RABBIT_CONNECTION_URL = process.env.RABBIT_CONNECTION_URL;
export const NODE_ENV = process.env.NODE_ENV;

// Departure
// Arrival
export const JOB_PREDICT_T2S_RESULT_EXCHANGE = process.env.JOB_PREDICT_T2S_RESULT_EXCHANGE;

export const JOB_STATUS_UPDATED_EXCHANGE = process.env.JOB_STATUS_UPDATED_EXCHANGE;

export const PORT = process.env.TEXT_TO_SPEECH_PORT;
export const JOB_WILL_PREDICT_T2S_QUEUE = process.env.JOB_WILL_PREDICT_T2S_QUEUE;
export const STORAGE_SERVICE_URL = process.env.STORAGE_SERVICE_URL;
