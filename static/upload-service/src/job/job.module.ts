import { Module } from '@nestjs/common';
import { JobController } from './job.controller';
import { JobService } from './job.service';
import { RabbitModule } from '../rabbit/rabbit.module';
import { AudioGetterFactory } from './utils/AudioGetterFactory';

import { RedisModule } from '../redis/redis.module';

@Module({
  controllers: [JobController],
  providers: [JobService, AudioGetterFactory],
  imports: [RedisModule],
})
export class JobModule {}
