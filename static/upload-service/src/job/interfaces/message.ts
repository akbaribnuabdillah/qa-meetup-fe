export interface SpeechToTextMessage {
    body: IncomingMessage;
    timestamp: number;
}

export interface IncomingMessage {
    requestId: string;
    channelId: string;
    message: Message;
    partnerId: string;
    options: object;
}

export type MessageType = 'text' | 'data' | 'command';

export interface Message {
    type: MessageType;
    content?: string;
    payload?: any;
    metadata?: any;
    id: string;
    event_time?: number;
}
