import { IncomingMessage } from '../interfaces';
export interface MessageDto {
    id: string;
    audioFile: string;
    messageBody: IncomingMessage;
    timestamp: number;
}
