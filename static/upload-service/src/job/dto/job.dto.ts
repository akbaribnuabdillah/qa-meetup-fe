export interface IJobDto {
    id: string;
    audioFile: string;
    channelId: string;
}

export interface IResultDto {
    id: string;
    result: string;
    channelId: string;
}

export interface IT2SJobDTO {
    id: string;
    message: string;
    channelId: string;
}

export interface IT2SJobPostDto {
    message: string;
    channelId: string;
}
