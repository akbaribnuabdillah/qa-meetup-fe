import { Controller, Post, Req, UseInterceptors, UploadedFile, Logger, Get, Param, Query, Res, Body } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { JobService } from './job.service';
import { IT2SJobPostDto } from './dto';
import { fileUploadOptions } from './job.config';
import { Request, Response } from 'express';

@Controller('jobs')
export class JobController {
    constructor(private readonly uploadService: JobService) {}

    /**
     * THIS METHOD MIGHT BE REMOVED SOON
     */
    @Post()
    @UseInterceptors(FileInterceptor('audio', fileUploadOptions))
    async create(@UploadedFile() file, @Req() req: Request, @Res() res: Response) {
        const { buffer, ...rest }: {buffer: Buffer} = file;
        const { channel_id } = req.body;
        Logger.log(rest);
        Logger.log(channel_id);
        const audioFile: string = buffer.toString('base64');
        const id  = await this.uploadService.create(audioFile, channel_id);
        // return res.send("success")
        const sse = setInterval(async () => {
            const { result, status } = await this.uploadService.findOne(id);
            if (status === 'failed') {
                res.status(500).json({
                    message: 'Translation failed',
                });
                clearInterval(sse);
            } else if (result != null) {
                console.log(result);
                res.set({
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                }).json({
                    message: result,
                })
                clearInterval(sse);
            }
        }, 250);
    }

    @Post('t2s')
    async createT2SJob(@Body() body: IT2SJobPostDto, @Res() res: Response) {
        const id = await this.uploadService.createT2S(body.message, body.channelId);
        const sse = setInterval(async () => {
            const { result, status } = await this.uploadService.findT2SJob(id);
            if (status === 'failed') {
                res.status(500).json({
                    message: 'Translation failed',
                });
                clearInterval(sse);
            } else if (result != null) {
                console.log(result);
                res.set({
                    'Cache-Control': 'no-cache',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                }).json({
                    fileUrl: result,
                });
                clearInterval(sse);
            }
        }, 250);
    }

    @Get(':id')
    async findOne(@Param('id') id) {
        try {
            const job = await this.uploadService.findOne(id);
            return job;
        } catch (e) {
            throw e;
        }
    }

    @Get()
    async findAll(@Query('limit') limit: number, @Query('page') page: number) {
        try {
            const jobs = await this.uploadService.findAll(limit, page);
            return jobs;
        } catch (e) {
            throw e;
        }
    }
}
