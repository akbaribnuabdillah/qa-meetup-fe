import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import { SpeechToTextMessage, IncomingMessage } from './interfaces';
import { IJobDto, MessageDto, IResultDto, IT2SJobDTO } from './dto';
import axios from 'axios';
import { RabbitService } from '../rabbit/rabbit.service';
import * as config from '../config';
import { AudioGetterFactory } from './utils/AudioGetterFactory';
import { RedisService } from '../redis/redis.service';
import urlJoin = require('url-join');

@Injectable()
export class JobService {
    constructor(
        private readonly amqpConnectionService: RabbitService,
        private readonly audioGetterFactory: AudioGetterFactory,
        private readonly redisService: RedisService,
        private readonly rabbitService: RabbitService,
    ) {
        this.rabbitService.consume(
            { queue: config.AUDIO_MESSAGE_FROM_EINSATZ_QUEUE, callback: this.handleAudioMessage },
            { queue: config.TEXT_MESSAGE_FROM_EINSATZ_QUEUE, callback: this.handleTextMessage },
        );
        this.rabbitService.subscribe(
            { exchange: config.JOB_PREDICT_RESULT_EXCHANGE, callback: this.sendPredictionResult },
        );
    }

    async create(audioFileBase64: string, channelId: string): Promise<string> {
        const willCreateQueue = config.JOB_WILL_CREATE_QUEUE;
        const jobId = uuidv4();
        const messageData: IJobDto = {
            id: jobId,
            channelId,
            audioFile: audioFileBase64,
        };
        this.rabbitService.sendToQueue(willCreateQueue, messageData);
        return jobId;
    }

    async createT2S(message: string, channelId: string) {
        const willCreateT2SQueue = config.JOB_WILL_CREATE_T2S_QUEUE;
        const jobId = uuidv4();
        const messageData: IT2SJobDTO = {
            id: jobId,
            message,
            channelId,
        }
        this.rabbitService.sendToQueue(willCreateT2SQueue, messageData);
        return jobId;
    }

    // readonly catchPredictionResult = async (result: JobResultDto) => {
    //     const { id, ...sentMessage } = result;
    //     this.rabbitService.sendToQueue(config.RESULT_MESSAGE_TO_EINSATZ_QUEUE, sentMessage);
    // }

    readonly sendPredictionResult = async (result: IResultDto) => {
        const { id, result: text } = result;
        const newMessage = await this.mixPredictionResultAndMessageFromRedis(id, text);
        this.rabbitService.sendToQueue(config.RESULT_MESSAGE_TO_EINSATZ_QUEUE, newMessage);
    }

    readonly handleAudioMessage = async (message: SpeechToTextMessage) => {
        const willCreateQueue = config.JOB_WILL_CREATE_QUEUE;
        const { body, timestamp } = message;
        const jobId = uuidv4();
        this.redisService.set(jobId, {
            body,
            timestamp,
        }, 86400); // One Day Expiration Time
        const audioUrl = body.message.payload.url;
        const audioGetter = this.audioGetterFactory.getAudioGetter(body.message.metadata.channelType);
        const audioBuffer = await audioGetter.generateAudioBuffer(body.message);
        // body = await this.generateAudioUrl(body);
        // const audioUrl = body.message.payload.url;
        // const { data: response }: { data: Buffer } = await axios.get(audioUrl, { responseType: 'arraybuffer' });
        const audioBase64 = audioBuffer.toString('base64');
        const channelId = body.channelId;
        const messageData: IJobDto = {
            id: jobId,
            channelId,
            audioFile: audioBase64,
        };
        this.rabbitService.sendToQueue(willCreateQueue, messageData);
    }

    readonly handleTextMessage = async (message: SpeechToTextMessage) => {
        const { body, timestamp } = message;
        const jobId = uuidv4();
        this.redisService.set(jobId, {
            body,
            timestamp,
        }, 86400); // One Day Expiration Time
        const channelId = body.channelId;
        const messageData: IT2SJobDTO = {
            id: jobId,
            message: body.message.content,
            channelId,
        };
        this.rabbitService.sendToQueue(config.JOB_WILL_CREATE_T2S_QUEUE, messageData);
    }

    // async generateAudioUrl(msgBody: IncomingMessage): Promise<IncomingMessage> {
    //     const { message } = msgBody;
    //     const { metadata, payload } = message;
    //     const channelType = metadata.channelType;
    //     let resultPayload: any;
    //     let url: string;
    //     switch (channelType) {
    //         case 'line':
    //             const messageId = payload.messageId;
    //             url = `https://api.line.me/v2/bot/message/${messageId}/content`;
    //             resultPayload = {
    //                 ...payload,
    //                 url,
    //             }
    //             break;
    //         case 'whatsapp':
    //             url = payload.link;
    //             resultPayload = {
    //                 ...payload,
    //                 url,
    //             }
    //             break;
    //         case 'telegram':
    //             const fileId = payload.fileId as string;
    //             const telegramAccessToken = metadata.telegramAccessToken as string;
    //             const getFileUrl = `https://api.telegram.org/bot${telegramAccessToken}/getFile?file_id=${fileId}`;
    //             const { data } = await axios.get<{file_path?: string}>(getFileUrl);
    //             const filePath = data.file_path;
    //             url = `https://api.telegram.org/file/bot${telegramAccessToken}/${filePath}`;
    //             resultPayload = {
    //                 ...payload,
    //                 url,
    //             }
    //             break;
    //         case 'fbmessenger':
    //             url = payload.url;
    //             resultPayload = {
    //                 ...payload,
    //                 url,
    //             }
    //             break;
    //         default:
    //             resultPayload = payload;
    //             break;
    //     }
    //     return {
    //         ...msgBody,
    //         message: {
    //             ...message,
    //             payload: resultPayload,
    //         },
    //     };
    // }

    async findOne(id: string) {
        try {
            const url = urlJoin(config.STORAGE_SERVICE_URL, 'jobs', id);
            const response = await axios.get(url);
            return response.data;
        } catch (e) {
            console.log(e)
            throw new HttpException(e.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    async findAll(limit: number = 10, page: number = 0) {
        try {
            const url = urlJoin(config.STORAGE_SERVICE_URL, 'jobs');
            const response = await axios.get(url, {
                params: {
                    limit,
                    page,
                },
            });
            return response.data;
        } catch (e) {
            throw new HttpException(e.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    async findT2SJob(id: string) {
        try {
            const url = urlJoin(config.STORAGE_SERVICE_URL, 'jobs/t2s', id);
            const response = await axios.get(url);
            return response.data;
        } catch (e) {
            throw new HttpException(e.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    private async mixPredictionResultAndMessageFromRedis(id: string, textResult: string): Promise<SpeechToTextMessage> {
        const message =  await this.redisService.get<SpeechToTextMessage>(id);
        const newMessage: SpeechToTextMessage = {
            ...message,
            body: {
                ...message.body,
                message: {
                    ...message.body.message,
                    type: 'text',
                    content: textResult,
                    payload: null,
                },
            },
        };
        return newMessage;
    }
}
