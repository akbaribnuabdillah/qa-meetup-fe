import axios, { AxiosInstance } from 'axios';
import { AudioGetter } from './AudioGetter';
import * as config from '../../config';

export class WhatsappAudioGetter extends AudioGetter {
    async generateAxiosRequest(metadata: any, payload: any): Promise<AxiosInstance> {
        const fileId = payload.id;
        const url = `${config.WHATSAPP_URL}/v1/media/${fileId}`;
        const token = metadata.whatsappAccessToken;
        return axios.create({
            url,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            responseType: 'arraybuffer',
        });
    }
}
