import { IncomingMessage } from '../interfaces';
import axios, { AxiosInstance } from 'axios';
import * as config from '../../config';
import { AudioGetter } from './AudioGetter';

export class LineAudioGetter extends AudioGetter {
    async generateAxiosRequest(metadata: any, payload: any): Promise<AxiosInstance> {
        const messageId = payload.messageId;
        const url = `${config.LINE_URL}/v2/bot/message/${messageId}/content`;
        const token = metadata.lineAccessToken;
        return axios.create({
            url,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            responseType: 'arraybuffer',
        });
    }
}
