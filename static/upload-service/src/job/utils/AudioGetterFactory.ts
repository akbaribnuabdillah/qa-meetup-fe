import { Injectable } from '@nestjs/common';
import { AudioGetter } from './AudioGetter';
import { LineAudioGetter } from './LineAudioGetter';
import { WhatsappAudioGetter } from './WhatsappAudioGetter';
import { TelegramAudioGetter } from './TelegramAudioGetter';
import { MessengerAudioGetter } from './MessengerAudioGetter';
import { SlackAudioGetter } from './SlackAudioGetter';

@Injectable()
export class AudioGetterFactory {
    private audioGetters: Map<string, AudioGetter>;

    constructor() {
        this.audioGetters = new Map();
        this.audioGetters.set(ChannelTypes.LINE, new LineAudioGetter());
        this.audioGetters.set(ChannelTypes.WHATSAPP, new WhatsappAudioGetter());
        this.audioGetters.set(ChannelTypes.TELEGRAM, new TelegramAudioGetter());
        this.audioGetters.set(ChannelTypes.MESSENGER, new MessengerAudioGetter());
        this.audioGetters.set(ChannelTypes.SLACK, new SlackAudioGetter());
    }

    getAudioGetter(channelType: string): AudioGetter {
        return this.audioGetters.get(channelType);
    }
}

enum ChannelTypes {
    LINE = 'line',
    WHATSAPP = 'whatsapp',
    TELEGRAM = 'telegram',
    MESSENGER = 'fbmessenger',
    SLACK = 'slack',
}
