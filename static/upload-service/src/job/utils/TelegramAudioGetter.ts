import axios, { AxiosInstance } from 'axios';
import * as config from '../../config';
import { AudioGetter } from './AudioGetter';
import { Logger } from '@nestjs/common';

export class TelegramAudioGetter extends AudioGetter {
    async generateAxiosRequest(metadata: any, payload: any): Promise<AxiosInstance> {
        const accessToken = metadata.telegramAccessToken;
        const fileId = payload.fileId;
        const getFileUrl = `${config.TELEGRAM_URL}/bot${accessToken}/getFile?file_id=${fileId}`;
        const { data } = await axios.get<{ok: boolean, result: {file_path: string}, description?: any}>(getFileUrl);
        if (!data.ok) {
            Logger.log(JSON.stringify(data.description));
            return null;
        }
        const fileUrl = `${config.TELEGRAM_URL}/file/bot${accessToken}/${data.result.file_path}`;
        return axios.create({
            url: fileUrl,
            responseType: 'arraybuffer',
        });
    }
}
