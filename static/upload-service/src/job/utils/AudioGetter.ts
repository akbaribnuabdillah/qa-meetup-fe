import { AxiosInstance } from 'axios';
import { Message } from '../interfaces';

export abstract class AudioGetter {
    async generateAudioBuffer(message: Message): Promise<Buffer> {
        const { metadata, payload } = message;
        const axiosInstance = await this.generateAxiosRequest(metadata, payload);
        const { data } = await axiosInstance.get<Buffer>('');
        return data;
    }

    abstract generateAxiosRequest(metadata: any, payload: any): Promise<AxiosInstance>;
}
