import axios, { AxiosInstance } from 'axios';
import { AudioGetter } from './AudioGetter';

export class MessengerAudioGetter extends AudioGetter {
    async generateAxiosRequest(_: any, payload: any): Promise<AxiosInstance> {
        const url = payload.url;
        return axios.create({
            url,
            responseType: 'arraybuffer',
        });
    }
}
