import axios, { AxiosInstance } from 'axios';
import { AudioGetter } from './AudioGetter';

export class SlackAudioGetter extends AudioGetter {
    async generateAxiosRequest(metadata: any, payload: any): Promise<AxiosInstance> {
        const url = payload.file.url_private;
        const token = metadata.slackAccessToken;
        return axios.create({
            url,
            headers: {
                Authorization: `Bearer ${token}`,
            },
            responseType: 'arraybuffer',
        });
    }
}
