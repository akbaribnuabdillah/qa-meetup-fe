import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { HttpException, HttpStatus } from '@nestjs/common';

export const fileUploadOptions: MulterOptions = {
    fileFilter: (_, file, cb) => {
        if (file) {
            if (file.mimetype.match(/^audio\/.*/g)) {
                cb(null, true);
            } else {
                cb(new HttpException(`Unsupported file type ${file.mimetype}. Please upload audio file only`, HttpStatus.BAD_REQUEST),
                    false);
            }
        } else {
            cb(new HttpException(`No file`, HttpStatus.BAD_REQUEST),
                    false);
        }
    },
};
