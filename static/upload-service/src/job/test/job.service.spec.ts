import { Test, TestingModule } from '@nestjs/testing';
import { JobService } from '../job.service';
import { IJob, SpeechToTextMessage } from '../interfaces';
import axios from 'axios';
import { RabbitService } from '../../rabbit/rabbit.service';
import * as config from '../../config';
import * as amqp from 'amqplib';
import { IJobDto, MessageDto } from '../dto';
import { RedisService } from '../../redis/redis.service';
import uuid = require('uuid');
import { AudioGetterFactory } from '../utils/AudioGetterFactory';
import { LineAudioGetter } from '../utils/LineAudioGetter';
import urlJoin = require('url-join');

jest.mock('uuid/v4');
jest.mock('../../rabbit/rabbit.service');
jest.mock('../../redis/redis.service');

describe('UploadService', () => {
  let service: JobService;
  let amqpService: RabbitService;
  let audioGetterFactory: AudioGetterFactory;
  let rabbitService: RabbitService;
  let redisService: RedisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JobService,
        RabbitService,
        AudioGetterFactory,
        RedisService,
      ],
    }).compile();

    service = module.get<JobService>(JobService);
    amqpService = module.get<RabbitService>(RabbitService);
    audioGetterFactory = module.get(AudioGetterFactory);
    rabbitService = module.get<RabbitService>(RabbitService);
    redisService = module.get(RedisService);
  });

  describe('find', () => {
    it('should have findOneById method', async () => {
      expect(service.findOne).toBeDefined();
    });
    it('should call GET to storage find URI with id 12345', async () => {
      const result: IJob = {
        id: '12345',
        configId: '345',
        status: 'pending',
        result: null,
        originalFileUrl: null,
        resampledFileUrl: null,
      };
      const spy = jest.spyOn(axios, 'get').mockImplementation(async () => ({data: result}));
      const findOne = await service.findOne('12345');
      const url = urlJoin(config.STORAGE_SERVICE_URL, 'jobs/12345');
      expect(spy).toHaveBeenCalledWith(url);
      expect(findOne).toEqual(result);
    });
    it('should call GET to storage find uri without id', async () => {
      const result: IJob[] = [{
        id: '12345',
        configId: '345',
        status: 'pending',
        result: null,
        originalFileUrl: null,
        resampledFileUrl: null,
      }];
      const spy = jest.spyOn(axios, 'get').mockImplementation(async () => ({data: result}));
      const findAll = await service.findAll();
      const url = urlJoin(config.STORAGE_SERVICE_URL, 'jobs');
      expect(spy).toHaveBeenCalledWith(url, {
        params: {
          limit: 10,
          page: 0,
        },
      });
      expect(findAll).toEqual(result);
    });
  });

  describe('job creation', () => {
    it('should subscribe to audio message queue and publish message', async () => {
<<<<<<< HEAD
      const lineAudioGetter = new LineAudioGetter();
      const audioBufferMock = jest.spyOn(lineAudioGetter, 'generateAudioBuffer').mockImplementation(async () => Buffer.from('mock'));
      const audioGetterGetMock = jest.spyOn(audioGetterFactory, 'getAudioGetter').mockImplementation(() => lineAudioGetter);
      const sendStub = jest.spyOn(amqpService, 'sendToQueue');
=======
      const getStub = jest.spyOn(axios, 'get').mockImplementation(async () => ({
        data: 'mock',
      }));
      const redisStub = jest.spyOn(redisService, 'set');
      const sendStub = jest.spyOn(rabbitService, 'sendToQueue');
>>>>>>> develop
      const mockedMessage: SpeechToTextMessage = {
        body: {
          requestId: 'mock',
          channelId: 'mock',
          message: {
            type: 'data',
            id: 'mock',
            payload: {
              type: 'audio',
              id: 'mockId',
            },
            metadata: {
              channelType: 'line',
              lineAccessToken: 'mockToken',
            },
          },
          partnerId: 'mock',
          options: {},
        },
        timestamp: 123213213,
      };
      const messageDtoMock: IJobDto = {
        id: 'mock',
        audioFile: Buffer.from('mock').toString('base64'),
        timestamp: 123213213,
        messageBody: mockedMessage.body,
        channelId: 'mock',
      }
      jest.spyOn(uuid, 'v4').mockReturnValue('mock');
      await service.handleAudioMessage(mockedMessage);
      expect(audioGetterGetMock).toHaveBeenCalledWith('line');
      expect(audioBufferMock).toHaveBeenCalledWith(mockedMessage.body.message);
      expect(sendStub).toHaveBeenCalledWith(config.JOB_WILL_CREATE_QUEUE, messageDtoMock);
      expect(redisStub).toHaveBeenCalledWith('mock', mockedMessage, 86400);
    });
  });

  describe('result', () => {
    it('should send to einsatz queue', async () => {
      const sendStub = jest.spyOn(rabbitService, 'sendToQueue');
      const getRedisStub = jest.spyOn(redisService, 'get');
      const mockedMessage: SpeechToTextMessage = {
        body: {
          requestId: 'mock',
          channelId: 'mock',
          message: {
            type: 'data',
            id: 'mock',
            payload: {
              type: 'audio',
              url: 'mock',
            },
          },
          partnerId: 'mock',
          options: {},
        },
        timestamp: 1234,
      };
      getRedisStub.mockImplementation(async () => mockedMessage);
      await service.sendPredictionResult({
        id: 'mock',
        result: 'halo',
        channelId: 'mock',
      });
      expect(getRedisStub).toHaveBeenCalledWith('mock');
      expect(sendStub).toHaveBeenCalledWith(config.RESULT_MESSAGE_TO_EINSATZ_QUEUE, {
        body: {
          requestId: 'mock',
          channelId: 'mock',
          message: {
            type: 'text',
            id: 'mock',
            content: 'halo',
            payload: null,
          },
          partnerId: 'mock',
          options: {},
        },
        timestamp: 1234,
      });
    });
  });
});
