import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { AudioGetter } from '../utils/AudioGetter';
import { LineAudioGetter } from '../utils/LineAudioGetter';
import { MessengerAudioGetter } from '../utils/MessengerAudioGetter';
import { SlackAudioGetter } from '../utils/SlackAudioGetter';
import { TelegramAudioGetter } from '../utils/TelegramAudioGetter';
import { WhatsappAudioGetter } from '../utils/WhatsappAudioGetter';
import * as config from '../../config';
import { AudioGetterFactory } from '../utils/AudioGetterFactory';
import Axios from 'axios';

describe('Audio Getter', () => {
    let lineAudioGetter: AudioGetter;
    let messengerAudioGetter: AudioGetter;
    let slackAudioGetter: AudioGetter;
    let telegramAudioGetter: AudioGetter;
    let whatsappAudioGetter: AudioGetter;
    let audioGetterFactory: AudioGetterFactory;
    let axiosCreateStub: jest.SpyInstance<AxiosInstance, [AxiosRequestConfig?]>;

    beforeEach(() => {
        lineAudioGetter = new LineAudioGetter();
        messengerAudioGetter = new MessengerAudioGetter();
        slackAudioGetter = new SlackAudioGetter();
        telegramAudioGetter = new TelegramAudioGetter();
        whatsappAudioGetter = new WhatsappAudioGetter();
        audioGetterFactory = new AudioGetterFactory();
        axiosCreateStub = jest.spyOn(axios, 'create');
    });

    afterEach(() => {
        axiosCreateStub.mockClear();
    });

    describe('Audio Getter Factory', () => {
        it('should return an AudioGetter (LINE)', () => {
            const audioGetter = audioGetterFactory.getAudioGetter('line');
            expect(audioGetter).toBeDefined();
            expect(audioGetter).toBeInstanceOf(LineAudioGetter);
            expect(audioGetter).toBeInstanceOf(AudioGetter);
        });
    });

    describe('Audio Getter', () => {
        it('should return Buffer of audio file', async () => {
            const axiosInstance = Axios.create();
            jest.spyOn(axiosInstance, 'get').mockImplementation(async () => ({
                data: Buffer.from('mock'),
            }));
            const axiosInstanceGetMock = jest.spyOn(lineAudioGetter, 'generateAxiosRequest').mockImplementation(async () => axiosInstance);
            const res = await lineAudioGetter.generateAudioBuffer({
                metadata: {
                    lineAccessToken: 'mock',
                },
                payload: {
                    id: 'mockId',
                },
                type: 'data',
                id: 'mockId',
            });
            expect(axiosInstanceGetMock).toHaveBeenCalledWith({ lineAccessToken: 'mock' }, { id: 'mockId' });
            expect(res).toEqual(Buffer.from('mock'));
        });
    });

    describe('Line Audio Getter', () => {
        it('should return axios instance', async () => {
            await lineAudioGetter.generateAxiosRequest({
                lineAccessToken: 'mockToken',
            }, {
                messageId: 'mockId',
            });
            const lineUrl = config.LINE_URL;
            expect(axiosCreateStub).toHaveBeenCalledWith({
                url: `${lineUrl}/v2/bot/message/mockId/content`,
                headers: {
                    Authorization: 'Bearer mockToken',
                },
                responseType: 'arraybuffer',
            });
        });
    });

    describe('Whatsapp Audio Getter', () => {
        it('should return axios instance', async () => {
            await whatsappAudioGetter.generateAxiosRequest({
                whatsappAccessToken: 'mockToken',
            }, {
                id: 'mockId',
            });
            const waUrl = config.WHATSAPP_URL;
            expect(axiosCreateStub).toHaveBeenCalledWith({
                url: `${waUrl}/v1/media/mockId`,
                headers: {
                    Authorization: 'Bearer mockToken',
                },
                responseType: 'arraybuffer',
            });
        });
    });

    describe('Messenger Audio Getter', () => {
        it('should return axios instance', async () => {
            await messengerAudioGetter.generateAxiosRequest({
                fbmessengerAccessToken: 'mockToken',
            }, {
                url: 'mockUrl',
            });
            expect(axiosCreateStub).toHaveBeenCalledWith({
                url: 'mockUrl',
                responseType: 'arraybuffer',
            });
        });
    });

    describe('Slack Audio Getter', () => {
        it('should return axios instance', async () => {
            await slackAudioGetter.generateAxiosRequest({
                slackAccessToken: 'mock-token',
            }, {
                file: {
                    url_private: 'mock-url',
                },
            });
            expect(axiosCreateStub).toHaveBeenCalledWith({
                url: 'mock-url',
                headers: {
                    Authorization: 'Bearer mock-token',
                },
                responseType: 'arraybuffer',
            })
        });
    });

    describe('Telegram Audio Getter', () => {
        it('should return axios instance', async () => {
            const axiosGetStub = jest.spyOn(axios, 'get').mockImplementation(async () => ({
                data: {
                    ok: true,
                    result: {
                        file_id: 'mockId',
                        file_size: 10,
                        file_path: 'mockPath',
                    },
                },
            }));
            await telegramAudioGetter.generateAxiosRequest({
                telegramAccessToken: 'mock-token',
            }, {
                fileId: 'mockId',
            });
            const telegramUrl = config.TELEGRAM_URL;
            expect(axiosGetStub).toHaveBeenCalledWith(`${telegramUrl}/botmock-token/getFile?file_id=mockId`);
            expect(axiosCreateStub).toHaveBeenCalledWith({
                url: `${telegramUrl}/file/botmock-token/mockPath`,
                responseType: 'arraybuffer',
            });
        });
    });
});
