import { Test, TestingModule } from '@nestjs/testing';
import { JobController } from '../job.controller';
import { JobService } from '../job.service';
import { IJob } from '../interfaces';
import { HttpException } from '@nestjs/common';
import { RabbitService } from '../../rabbit/rabbit.service';
import { AudioGetterFactory } from '../utils/AudioGetterFactory';
import { RedisService } from '../../redis/redis.service';

jest.mock('../../rabbit/rabbit.service');
jest.mock('../../redis/redis.service');

describe('Upload Controller', () => {
  let controller: JobController;
  let service: JobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JobController],
      providers: [
        JobService,
        RabbitService,
        AudioGetterFactory,
        RedisService,
      ],
    }).compile();

    controller = module.get<JobController>(JobController);
    service = module.get<JobService>(JobService);
  });

  describe('find', () => {
    it('should return all jobs', async () => {
      const result: IJob[] = [
          {
            id: '12345',
            configId: '345',
            status: 'pending',
            result: null,
            originalFileUrl: null,
            resampledFileUrl: null,
          },
          {
            id: '12345',
            configId: '345',
            status: 'success',
            result: 'Lorem',
            originalFileUrl: 'S3_url',
            resampledFileUrl: 'S3_url',
          },
      ];
      const spySuccess = jest.spyOn(service, 'findAll').mockImplementation(async () => result);
      const find = await controller.findAll(2, 0);
      expect(spySuccess).toHaveBeenCalled();
      expect(find).toBe(result);
    });

    it('findAll should throw HttpException error', async () => {
      jest.spyOn(service, 'findAll').mockImplementation(() => { throw new HttpException('Err', 401); });
      try {
        await controller.findAll(2, 0);
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('findOne should throw HttpException error', async () => {
      jest.spyOn(service, 'findOne').mockImplementation(() => { throw new HttpException('Err', 401); });
      try {
        await controller.findOne('12345');
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('should return a job with id 12345', async () => {
      const result: IJob = {
        id: '12345',
        configId: '345',
        status: 'pending',
        result: null,
        originalFileUrl: null,
        resampledFileUrl: null,
      };
      const spy = jest.spyOn(service, 'findOne').mockImplementation(async () => result);
      const find = await controller.findOne('12345');
      expect(spy).toHaveBeenCalled();
      expect(find).toBe(result);
    });
  });
});
