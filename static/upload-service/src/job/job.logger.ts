import { Logger } from '@nestjs/common';

export class JobLogger extends Logger {
    getDateNow(date: Date) {
        return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
    }
    getTimeNow(date: Date) {
        return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
    }
    getLoggingStructure(message: any, date: Date, level: string, context?: string) {
        return `${this.getDateNow(date)} ${this.getTimeNow(date)} ${context}/${level} ${message}`;
    }
    log(message: any, context: string = null) {
        const date = new Date();
        super.log(this.getLoggingStructure(message, date, 'log', context));
    }
    error(message: any, trace?: string, context: string = null) {
        const date = new Date();
        super.error(`${this.getLoggingStructure(message, date, 'error', context)}\n${trace}`);
    }
    warn(message: any, context?: string) {
        const date = new Date();
        super.warn(this.getLoggingStructure(message, date, 'warning', context));
    }
}
