import { Module } from '@nestjs/common';
import { UserConfigController } from './user-config.controller';
import { UserConfigService } from './user-config.service';
import { RabbitModule } from '../rabbit/rabbit.module';

@Module({
  controllers: [UserConfigController],
  providers: [UserConfigService],
})
export class UserConfigModule {}
