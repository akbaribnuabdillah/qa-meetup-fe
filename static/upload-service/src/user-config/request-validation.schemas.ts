import * as joi from '@hapi/joi';

const googleServiceAccountValidationSchema = joi.object().keys({
    type: joi.string().required(),
    project_id: joi.string().required(),
    private_key_id: joi.string().required(),
    private_key: joi.string().required(),
    client_email: joi.string().required(),
    client_id: joi.string().required(),
    auth_uri: joi.string().required(),
    token_uri: joi.string().required(),
    auth_provider_x509_cert_url: joi.string().required(),
    client_x509_cert_url: joi.string().required(),
});

const bahasakitaAccountValidationSchema = joi.object().keys({
    base_64_encoded_credentials: joi.string().required(),
});

const baseUserConfigValidationSchema = {
    projectId: joi.string().required(),
    provider: joi.string().valid('google', 'bahasakita').required(),
};

export const createConfigWithGoogleValidationSchema = joi.object().keys({
    ...baseUserConfigValidationSchema,
    accountData: googleServiceAccountValidationSchema,
});

export const createConfigWithBahasakitaValidationSchema = joi.object().keys({
    ...baseUserConfigValidationSchema,
    accountData: bahasakitaAccountValidationSchema,
});

export const updateConfigWithGoogleValidationSchema = joi.object().keys({
    provider: baseUserConfigValidationSchema.provider,
    accountData: googleServiceAccountValidationSchema,
});

export const updateConfigWithBahasakitaValidationSchema = joi.object().keys({
    provider: baseUserConfigValidationSchema.provider,
    accountData: bahasakitaAccountValidationSchema,
});
