import { IBahasaKitaAccount, IGoogleServiceAccount } from './';

export interface IUserConfig {
    id: string;
    projectId: string;
    provider: 'google' | 'bahasakita';
    accountData: IGoogleServiceAccount | IBahasaKitaAccount;
}
