import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { IUserConfigDto } from './dto';
import * as uuidv4 from 'uuid/v4';
import { IUserConfig } from './interfaces';
import axios from 'axios';
import { RabbitService } from '../rabbit/rabbit.service';
import * as configFile from '../config';

@Injectable()
export class UserConfigService {

    constructor(
        private readonly amqpConnectionService: RabbitService,
    ) {}

    async create(config: IUserConfigDto) {
        const queue = configFile.CONFIG_CREATE_QUEUE;
        const userConfig: IUserConfig = { ...config, id: uuidv4() };
        this.amqpConnectionService.sendToQueue(queue, userConfig);
        return userConfig;
    }

    async findOne(configId: string) {
        try {
            const response = await axios.get(`${configFile.STORAGE_SERVICE_URL}/project/configs/${configId}`);
            return response.data;
        } catch (e) {
            throw new HttpException(e.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    async findByUserId(projectId: string, limit: number = 10, page: number = 0) {
        try {
            const response = await axios.get(`${configFile.STORAGE_SERVICE_URL}/project/${projectId}/configs`, {
                params: {
                    limit,
                    page,
                },
            });
            return response.data;
        } catch (e) {
            throw new HttpException(e.message, HttpStatus.SERVICE_UNAVAILABLE);
        }
    }

    async update(configId: string, config: Partial<IUserConfigDto>) {
        const queue = configFile.CONFIG_UPDATE_QUEUE;
        const updatedUserConfig = { ...config, id: configId };
        this.amqpConnectionService.sendToQueue(queue, updatedUserConfig);
        return updatedUserConfig;
    }

    async delete(configId: string) {
        const queue = configFile.CONFIG_DELETE_QUEUE;
        const deleteDto = { id: configId };
        this.amqpConnectionService.sendToQueue(queue, deleteDto);
        return deleteDto;
    }

    async connectConfigToChannel(configId: string, channelId: string) {
        const queue = configFile.CONFIG_CHANNEL_CONNECT_QUEUE;
        const connectDto = { id: configId, channelId };
        this.amqpConnectionService.sendToQueue(queue, connectDto);
        return connectDto;
    }
}
