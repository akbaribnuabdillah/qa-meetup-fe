import { ArgumentMetadata, Injectable, PipeTransform, BadRequestException, Logger } from '@nestjs/common';
import * as joi from '@hapi/joi';
import { IUserConfigDto } from '../dto';

@Injectable()
export class ConfigValidationPipe implements PipeTransform {
  constructor(
    private readonly googleConfigSchema: joi.SchemaLike,
    private readonly bahasaKitaConfigSchema: joi.SchemaLike,
  ) {}
  transform(value: any, metadata: ArgumentMetadata) {
    const input = value as IUserConfigDto;
    const { error } = input.provider === 'google' ? joi.validate(input, this.googleConfigSchema) :
                                                joi.validate(input, this.bahasaKitaConfigSchema);
    if (error) {
      throw new BadRequestException(error.message);
    }

    return value;
  }
}
