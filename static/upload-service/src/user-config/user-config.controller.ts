import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Delete,
  Put,
  UsePipes,
  Query,
} from '@nestjs/common';
import { UserConfigService } from './user-config.service';
import { IUserConfigDto } from './dto';
import { ConfigValidationPipe } from './pipes/config-validation.pipe';
import {
  createConfigWithGoogleValidationSchema,
  createConfigWithBahasakitaValidationSchema,
  updateConfigWithGoogleValidationSchema,
  updateConfigWithBahasakitaValidationSchema,
} from './request-validation.schemas';

@Controller('project')
export class UserConfigController {
  constructor(private readonly userConfigService: UserConfigService) {}

  @Post('configs')
  @UsePipes(
    new ConfigValidationPipe(
      createConfigWithGoogleValidationSchema,
      createConfigWithBahasakitaValidationSchema,
    ),
  )
  async create(@Body() config: IUserConfigDto) {
    return await this.userConfigService.create(config);
  }

  @Get('configs/:id')
  async findOne(
    @Param('id') configId: string,
  ) {
    return await this.userConfigService.findOne(configId);
  }

  @Get(':projectId/configs')
  async findByProjectId(@Param('projectId') projectId: string, @Query('limit') limit: number, @Query('page') page: number) {
    return this.userConfigService.findByUserId(projectId, limit, page);
  }

  @Delete('configs/:id')
  async delete(@Param('id') configId: string) {
    return await this.userConfigService.delete(configId);
  }

  @Put('configs/:id')
  async update(
    @Param('id') configId: string,
    @Body(
      new ConfigValidationPipe(
        updateConfigWithGoogleValidationSchema,
        updateConfigWithBahasakitaValidationSchema,
      ),
    )
    updateConfig: Partial<IUserConfigDto>,
  ) {
    return await this.userConfigService.update(configId, updateConfig);
  }

  @Put('configs/:id/channel/:channelId')
  async connectConfigToChannel(
    @Param('id') configId: string,
    @Param('channelId') channelId: string,
  ) {
    return await this.userConfigService.connectConfigToChannel(configId, channelId);
  }

}
