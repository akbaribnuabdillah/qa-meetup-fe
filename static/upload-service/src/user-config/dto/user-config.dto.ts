import { IBahasaKitaAccount, IGoogleServiceAccount } from '../interfaces';

export interface IUserConfigDto {
    projectId: string;
    provider: 'google'|'bahasakita';
    accountData: IGoogleServiceAccount | IBahasaKitaAccount;
}
