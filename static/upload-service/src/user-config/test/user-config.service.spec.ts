import { Test, TestingModule } from '@nestjs/testing';
import { UserConfigService } from '../user-config.service';
import { IUserConfig } from '../interfaces';
import axios from 'axios';
import { RabbitService } from '../../rabbit/rabbit.service';
import * as config from '../../config';
import { IUserConfigDto } from '../dto';
import urlJoin = require('url-join');

jest.mock('uuid/v4', () => jest.fn().mockReturnValue('mock'));
jest.mock('../../rabbit/rabbit.service');

describe('UserConfigService', () => {
  let service: UserConfigService;
  let amqpService: RabbitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserConfigService,
        RabbitService,
      ],
    }).compile();

    service = module.get<UserConfigService>(UserConfigService);
    amqpService = module.get<RabbitService>(RabbitService);
  });

  describe('find', () => {
    it('should return with user config with ID 12345 and user ID 34567', async () => {
      const result: IUserConfig = {
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'thisismockencodedcredential',
          entity: 'kata'
        },
      };
      const spy = jest.spyOn(axios, 'get').mockImplementation(async (uri: string) => ({data: result}));
      const find = await service.findOne('12345');
      const url = urlJoin(config.STORAGE_SERVICE_URL, 'project/configs/12345');
      expect(spy).toHaveBeenCalledWith(url);
      expect(find).toBe(result);
    });

    it('should return user configs with user ID 34567', async () => {
      const result: IUserConfig[] = [{
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'thisismockencodedcredential',
          entity: 'kata',
        },
      }];
      const spy = jest.spyOn(axios, 'get').mockImplementation(async (uri: string) => ({data: result}));
      const find = await service.findByUserId('34567', 2, 0);
      const url = urlJoin(config.STORAGE_SERVICE_URL, 'project/34567/configs');
      expect(spy).toHaveBeenCalledWith(url , {
        params: {
          limit: 2,
          page: 0,
        },
      });
      expect(find).toBe(result);
    });
  });

  describe('CUD', () => {
    it('create user config should send to queue', async () => {
      const sendStub = jest.spyOn(amqpService, 'sendToQueue');
      const userConfigMock: IUserConfigDto = {
        projectId: 'mock',
        provider: 'google',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      };
      await service.create(userConfigMock);
      expect(sendStub).toHaveBeenCalledWith(
        config.CONFIG_CREATE_QUEUE,
        {
          ...userConfigMock,
          id: 'mock',
        },
      );
    });

    it('update user config should send to queue', async () => {
      const sendStub = jest.spyOn(amqpService, 'sendToQueue');
      const updateUserConfigMock: Partial<IUserConfigDto> = {
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      };
      await service.update('mock', updateUserConfigMock);
      expect(sendStub).toHaveBeenCalledWith(
        config.CONFIG_UPDATE_QUEUE,
        {
          ...updateUserConfigMock,
          id: 'mock',
        },
      );
    });

    it('delete user config should send to queue', async () => {
      const sendStub = jest.spyOn(amqpService, 'sendToQueue');
      const deleteUserConfig = {
        id: 'mock',
      };
      await service.delete('mock');
      expect(sendStub).toHaveBeenCalledWith(config.CONFIG_DELETE_QUEUE, deleteUserConfig);
    });

    it('connect to channel should send to queue', async () => {
      const sendStub = jest.spyOn(amqpService, 'sendToQueue');
      const mockConnectDto = { id: 'mock', channelId: 'mock' };
      await service.connectConfigToChannel('mock', 'mock');
      expect(sendStub).toHaveBeenCalledWith(config.CONFIG_CHANNEL_CONNECT_QUEUE, mockConnectDto);
    });
  });
});
