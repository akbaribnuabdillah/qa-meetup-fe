import { Test, TestingModule } from '@nestjs/testing';
import { UserConfigController } from '../user-config.controller';
import { UserConfigService } from '../user-config.service';
import { IUserConfig } from '../interfaces';
import { IUserConfigDto } from '../dto';
import { RabbitService } from '../../rabbit/rabbit.service';

jest.mock('../../rabbit/rabbit.service');

describe('UserConfig Controller', () => {
  let controller: UserConfigController;
  let service: UserConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserConfigController],
      providers: [
        UserConfigService,
        RabbitService,
      ],
    }).compile();

    controller = module.get<UserConfigController>(UserConfigController);
    service = module.get<UserConfigService>(UserConfigService);
  });

  describe('create', () => {
    it('should return config', async () => {
      const input: IUserConfigDto = {
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'thisismockencodedcredential',
          entity: 'kata',
        },
      };
      const output: IUserConfig = {
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'thisismockencodedcredential',
          entity: 'kata',
        },
      };
      const spy = jest.spyOn(service, 'create').mockImplementation(async (_: IUserConfigDto) => output);
      const create = await controller.create(input);
      expect(spy).toHaveBeenCalledWith(input);
      expect(create).toBe(output);
    });
  });

  describe('find', () => {
    it('should return a user config with ID 12345', async () => {
      const result: IUserConfig = {
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'thisismockencodedcredential',
          entity: 'kata',
        },
      };
      const spy = jest.spyOn(service, 'findOne').mockImplementation(async (id: string) => result);
      const find = await controller.findOne('12345');
      expect(spy).toHaveBeenCalledWith('12345');
      expect(find).toBe(result);
    });

    it('should return user configurtions with user ID 34567', async () => {
      const result: IUserConfig[] = [{
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'thisismockencodedcredential',
          entity: 'kata',
        },
      }];
      const spy = jest.spyOn(service, 'findByUserId').mockImplementation(async () => result);
      const findByUserId = await controller.findByProjectId('34567', 5, 0);
      expect(spy).toHaveBeenCalledWith('34567', 5, 0);
      expect(findByUserId).toBe(result);
    })
  });

  describe('delete', () => {
    it('should return ID of deleted config', async () => {
      const spy = jest.spyOn(service, 'delete').mockImplementation(async () => ({ id: '12345' }));
      const remove = await controller.delete('12345');
      expect(spy).toHaveBeenCalledWith('12345');
      expect(remove).toEqual({ id: '12345' });
    });
  });

  describe('update', () => {
    it('should return ID and updated fields', async () => {
      const result = { id: '34567', config: { base_64_encoded_credential: '23123213', entity: 'kata' } };
      const spy = jest.spyOn(service, 'update').mockImplementation(async () => result);
      const update = await controller.update('34567', { accountData: { base_64_encoded_credential: '23123213', entity: 'kata' } });
      expect(spy).toHaveBeenCalledWith('34567', { accountData: { base_64_encoded_credential: '23123213', entity: 'kata' } });
      expect(update).toBe(result);
    });

    it('should connect config with config ID to channel with channel ID', async () => {
      const result = { id: '12345', channelId: '34567' };
      const spy = jest.spyOn(service, 'connectConfigToChannel').mockImplementation(async () => result);
      const connect = await controller.connectConfigToChannel('12345', '34567');
      expect(spy).toHaveBeenCalledWith('12345', '34567');
      expect(connect).toBe(result);
    })
  });
});
