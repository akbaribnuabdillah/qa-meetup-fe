import { Module } from '@nestjs/common';
import { JobModule } from './job/job.module';
import { UserConfigModule } from './user-config/user-config.module';
import { RabbitModule } from './rabbit/rabbit.module';
import { RedisModule } from './redis/redis.module';

@Module({
  imports: [JobModule, UserConfigModule, RabbitModule, RedisModule],
})
export class AppModule {}
