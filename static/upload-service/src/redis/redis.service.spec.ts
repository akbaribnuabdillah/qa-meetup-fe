import { RedisService } from './redis.service';
import * as redis from 'redis';
import * as config from '../config';

describe('RedisService', () => {
  let service: RedisService;

  // @ts-ignore
  const mockRedisClient: jest.Mocked<redis.RedisClient> = {
    get: jest.fn(),
    set: jest.fn(),
    on: jest.fn(),
  };

  beforeEach(async () => {
    jest.spyOn(redis, 'createClient').mockReturnValue(mockRedisClient);
    service = new RedisService();
  });

  it('should call redis createClient', () => {
    expect(redis.createClient).toHaveBeenCalledWith({
      url: config.REDIS_URL,
    });
  });

  it('should call redis\' set function', async () => {
    await service.set('mock', 'mock', 1200);
    expect(mockRedisClient.set).toHaveBeenCalledWith('mock', JSON.stringify('mock'), 'EX', 1200);
  });
});
