import { Injectable, Logger } from '@nestjs/common';
import * as redis from 'redis';
import * as config from '../config';

interface GetMissParam<T> {
    missCallback: () => Promise<T>;
    expireDurationInSecs?: number;
}
@Injectable()
export class RedisService {
    client: redis.RedisClient;

    constructor() {
        this.client = redis.createClient({
            url: config.REDIS_URL,
        });
        this.client.on('error', (err) => {
            Logger.error(err.message);
        });
    }

    set<T>(key: string, data: T, expireDurationInSecs: number = 60) {
        this.client.set(key, JSON.stringify(data), 'EX', expireDurationInSecs);
    }

    get<T>(key: string, missParam?: GetMissParam<T>): Promise<T> {
        return new Promise((resolve, reject) => {
            this.client.get(key, async (err, reply) => {
                if (err) {
                    reject(err);
                } else {
                    if (reply) {
                        const data: T = JSON.parse(reply);
                        resolve(data);
                    } else {
                        if (missParam) {
                            const { missCallback, expireDurationInSecs = 60 } = missParam;
                            const data = await missCallback();
                            this.set(key, data, expireDurationInSecs);
                            resolve(data);
                        }
                        resolve(null);
                    }
                }
            });
        });
    }
}
