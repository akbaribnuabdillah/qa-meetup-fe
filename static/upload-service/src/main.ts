import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { JobLogger } from './job/job.logger';
import { PORT } from './config';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {logger: JobLogger});
  app.enableCors();
  Logger.log(`Upload Service starts at port ${PORT}`);
  await app.listen(PORT);
}
bootstrap();
