import * as dotenv from 'dotenv';

dotenv.config();

export const RABBIT_CONNECTION_URL = process.env.RABBIT_CONNECTION_URL;
export const STORAGE_SERVICE_URL = process.env.STORAGE_SERVICE_URL;
export const BAHASAKITA_OAUTH_URL = 'https://oauth.bahasakita.co.id/api/token';
export const BAHASAKITA_SPEECH_API_URL = 'https://api-dev.bahasakita.co.id/speech';
export const NODE_ENV = process.env.NODE_ENV;
export const PORT = process.env.PREDICT_SERVICE_PORT;

// Arrival
export const JOB_RESAMPLE_RESULT_EXCHANGE = process.env.JOB_RESAMPLE_RESULT_EXCHANGE;

// Departure
export const JOB_PREDICT_RESULT_EXCHANGE = process.env.JOB_PREDICT_RESULT_EXCHANGE;
export const JOB_STATUS_UPDATE_QUEUE = process.env.JOB_STATUS_UPDATE_QUEUE;
