
export interface JobDto {
    id: string;
    audioFile: string;
    configId: string;
    sampleRate: number;
    audioFormat: string;
    channelId: string;
}
