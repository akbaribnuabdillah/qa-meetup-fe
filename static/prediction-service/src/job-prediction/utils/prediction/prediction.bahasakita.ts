import { Injectable, Logger } from '@nestjs/common';
import axios from 'axios';
import * as https from 'https';
import * as configFile from '../../../config';
import * as uuidv4 from 'uuid/v4';
import * as _ from 'lodash';
import { Predictor } from '../../interfaces/predictor';
import { IBahasaKitaAccount } from '../../interfaces/account';

export class BahasaKitaNotula implements Predictor {

    async predict(jobId: string, audioFile: string, account: IBahasaKitaAccount): Promise<string> {
        const sessionId = uuidv4();
        const { base_64_encoded_credential, entity } = account;
        const accessToken = await this.getToken(base_64_encoded_credential);
        const streamSetup = this.streamSetup(entity, sessionId, accessToken);
        const { code: initCode } = await streamSetup('init');
        try {
            if (initCode === 0) {
                const { code: bosCode, data } = await streamSetup('bos');
                if (bosCode === 0) {
                    const utteranceId = data.utterance_id;
                    const audioBuffer = Buffer.from(audioFile, 'base64');
                    const text = await this.audioStreaming(entity, sessionId, accessToken)(utteranceId, audioBuffer);
                    Logger.log(`Job with ID ${jobId} translates into: ${text}`);
                    return text;
                }
            }
        } catch (err) {
            Logger.error(err.message);
        }
    }

    private streamSetup(entityName: string, sessionId: string, token: string) {
        return async (cmd: 'init' | 'bos') => {
            const stanUuid = uuidv4();
            try {
                const { data } = await axios.post(configFile.BAHASAKITA_SPEECH_API_URL, {
                    bk: {
                        cmd,
                        entity: entityName,
                        stan: stanUuid,
                        time: Date.now(),
                        protocol: 'stream',
                        version: '1.0',
                        type: 0,
                        data: {
                            session_id: sessionId,
                        },
                    },
                }, {
                    headers: {
                        'content_type': 'application/json',
                        'Accept-Charset': 'UTF-8',
                        'Authorization': `Bearer ${token}`,
                    },
                });
                return data.bk;
            } catch (err) {
                throw err;
            }
        };
    }

    private audioStreaming(entityName: string, sessionId: string, token: string) {
        return async (utteranceId: string, audioFileBuffer: Buffer) => {
            try {
                let resultText = [];
                let uttId = utteranceId;
                const audioArr = audioFileBuffer.slice(44);
                const chunk = 6400;
                for (let i = 0, j = audioArr.length; i < j; i += chunk) {
                    const stanUuid = uuidv4();
                    const tempAudioArr = audioArr.slice(i, i + chunk);
                    const audioBase64 = tempAudioArr.toString('base64');
                    const { data: sound } = await axios.post(configFile.BAHASAKITA_SPEECH_API_URL, {
                        bk: {
                            cmd: 'audio',
                            entity: entityName,
                            stan: stanUuid,
                            time: Date.now(),
                            protocol: 'stream',
                            version: '1.0',
                            type: 0,
                            data: {
                                session_id: sessionId,
                                offset: i,
                                len: tempAudioArr.length,
                                audio: audioBase64,
                                utterance_id: uttId,
                            },
                        },
                    }, {
                        headers: {
                            'content_type': 'application/json',
                            'Accept-Charset': 'UTF-8',
                            'Authorization': `Bearer ${token}`,
                        },
                    });
                    const nextUtteranceId = sound.bk.data.next_utterance_id;
                    if (nextUtteranceId) {
                        uttId = nextUtteranceId;
                    }
                    const textArr = sound.bk.data.text;
                    _.forEach(textArr, o => {
                        if (o.type === 'final' && o.value) {
                            resultText.push(o.value);
                        }
                    });
                }
                return resultText.join(' ');
            } catch (err) {
                throw err;
            }
        };
    }

    private async getToken(base64Creds: string): Promise<string> {
        const { data: token } = await axios.post(configFile.BAHASAKITA_OAUTH_URL, {
            grant_type: 'client_credentials',
            scope: 'SpeechTest',
        }, {
            headers: {
                Authorization: `Basic ${base64Creds}`,
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false }),
            timeout: 25000,
        });
        return token.access_token;
    }
}