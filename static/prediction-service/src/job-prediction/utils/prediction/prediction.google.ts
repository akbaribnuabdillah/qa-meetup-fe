import { Injectable, Logger } from '@nestjs/common';
import { IGoogleServiceAccount } from '../../interfaces/account';
import { IRecognitionConfig } from '../../interfaces/recognition.config';
import { Predictor } from './../../interfaces/predictor';
import * as stream from 'stream';

export class GoogleSpeechToText implements Predictor {
    private readonly mRecognitionConfig: IRecognitionConfig;

    constructor(recognitionConfig: IRecognitionConfig) {
        this.mRecognitionConfig = recognitionConfig;
    }

    async predict(jobId: string, audioFile: string, account: any, predictionConfig: any): Promise<string> {
        const speech = await import('@google-cloud/speech');
        const client = new speech.SpeechClient({
            credentials: account,
        });
        const audioBuffer = Buffer.from(audioFile, 'base64');
        const audioStream = new stream.PassThrough();
        audioStream.end(audioBuffer);
        // };
        const config = {
            ...this.mRecognitionConfig,
            ...predictionConfig,
        };
        const request = {
            // audio,
            config,
        };
        return new Promise((resolve, reject) => {
            try {
                console.time("Translate")
                const recognizeStream = client
                    .streamingRecognize(request)
                    .on('error', Logger.error)
                    .on('data', (data: any) => {
                        console.timeEnd("Translate")
                        const transcript: string = data.results[0].alternatives[0].transcript;
                        Logger.log(`Job with ID ${jobId} translates into: ${transcript}`);
                        resolve(transcript);
                    });
                audioStream.pipe(recognizeStream);
            } catch (ex) {
                Logger.error(ex.message);
                reject(ex);
            }
        });
    }
}
