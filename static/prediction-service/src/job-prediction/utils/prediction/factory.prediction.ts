import { Predictor } from '../../interfaces/predictor';
import { GoogleSpeechToText } from './prediction.google';
import { BahasaKitaNotula } from './prediction.bahasakita';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PredictionFactory {
    private providerPredictionMap: Map<string, Predictor>;

    constructor() {
        this.providerPredictionMap = new Map();
        this.providerPredictionMap.set('google', new GoogleSpeechToText({
            languageCode: 'id-ID',
            alternativeLanguageCodes: ['id-ID', 'en-US'],
            metadata: {
                microphoneDistance: 'NEARFIELD',
                originalMediaType: 'AUDIO',
                recordingDeviceType: 'SMARTPHONE',
                interactionType: 'VOICE_SEARCH',
            },
        }));
        this.providerPredictionMap.set('bahasakita', new BahasaKitaNotula());
    }

    public getPredictor(provider: string): Predictor {
        return this.providerPredictionMap.get(provider);
    }
}