import { Injectable, Logger } from '@nestjs/common';
import { RabbitService } from '../rabbit/rabbit.service';
import * as amqp from 'amqplib';
import { IUserConfigDto, JobDto } from './dto';
import axios from 'axios';
import * as configFile from '../config';
import { PredictionFactory } from './utils/prediction/factory.prediction';
import { JobResult } from './interfaces/job';

@Injectable()
export class JobPredictionService {
    constructor(
        private readonly rabbitService: RabbitService,
        private readonly predictionFactory: PredictionFactory,
    ) {
        this.rabbitService.subscribe(
            { exchange: configFile.JOB_RESAMPLE_RESULT_EXCHANGE, callback: this.predict },
        );
    }

    readonly predict = async (resampleResult: JobDto) => {
        const { id, audioFile, configId, channelId } = resampleResult;
        const { provider, accountData } = await this.mGetUserConfig(configId);
        let predictionConfig: any;
        if (provider === 'google') {
            predictionConfig = {
                encoding: resampleResult.audioFormat,
                sampleRateHertz: resampleResult.sampleRate,
            };
        }
        try {
            const predictor = this.predictionFactory.getPredictor(provider);
            const predictionResult = await predictor.predict(id, audioFile, accountData, predictionConfig);
            const data: JobResult = {
                id,
                result: predictionResult,
                channelId,
            };
            this.rabbitService.publish(configFile.JOB_PREDICT_RESULT_EXCHANGE, data);
            this.rabbitService.sendToQueue(configFile.JOB_STATUS_UPDATE_QUEUE, { id, status: 'success' });
        } catch (err) {
            const data: JobResult = {
                id,
                result: null,
                channelId,
            };
            Logger.error(err.message);
            this.rabbitService.publish(configFile.JOB_PREDICT_RESULT_EXCHANGE, data);
            this.rabbitService.sendToQueue(configFile.JOB_STATUS_UPDATE_QUEUE, { id, status: 'failed' });
        }
    }

    async mGetUserConfig(configId: string) {
        const uri = `${configFile.STORAGE_SERVICE_URL}/project/configs/${configId}`;
        const { data } = await axios.get(uri);
        const userConfig: IUserConfigDto = data as IUserConfigDto;
        return userConfig;
    }

}
