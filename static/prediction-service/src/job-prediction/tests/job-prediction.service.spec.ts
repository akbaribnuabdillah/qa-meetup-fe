import { Test, TestingModule } from '@nestjs/testing';
import { JobPredictionService } from '../job-prediction.service';
import { RabbitService } from '../../rabbit/rabbit.service';
import { PredictionFactory } from '../utils/prediction/factory.prediction';
import { Predictor } from '../interfaces/predictor';
import * as config from '../../config';
import { IUserConfigDto, JobDto } from '../dto';
import axios from 'axios';
import * as amqp from 'amqplib';

jest.mock('../../rabbit/rabbit.service');

describe('JobPredictionService', () => {
  let service: JobPredictionService;
  let factory: PredictionFactory;
  let rabbitService: jest.Mocked<RabbitService>;

  // @ts-ignore
  const mockedChannel: jest.Mocked<amqp.Channel> = {
    assertExchange: jest.fn(),
    assertQueue: jest.fn().mockReturnValue({
      queue: 'mock',
    }),
    bindQueue: jest.fn(),
    consume: jest.fn(),
    ack: jest.fn(),
    publish: jest.fn(),
  };

  beforeEach(async () => {

    // @ts-ignore
    jest.spyOn(amqp, 'connect').mockImplementation(async () => ({
      createChannel: jest.fn().mockImplementation(async () => mockedChannel),
    }));
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JobPredictionService,
        RabbitService,
        PredictionFactory,
      ],
    }).compile();
    service = module.get<JobPredictionService>(JobPredictionService);
    factory = module.get(PredictionFactory);
    rabbitService = module.get(RabbitService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('prediction', () => {
    const fakeConfig: IUserConfigDto = {
      id: 'mock',
      projectId: 'mock',
      provider: 'google',
      accountData: {
        base_64_encoded_credential: 'mock',
        entity: 'mock',
      },
    };
    const fakeResampleResult: JobDto = {
      id: 'mock',
      configId: 'mock',
      audioFile: 'mock',
      audioFormat: "FLAC",
      sampleRate: 10000,
      channelId: 'mock',
    };

    it('should predict with Google and send the result and status to queue', async () => {
      const configStub = jest.spyOn(service, 'mGetUserConfig').mockImplementation(async () => fakeConfig);
      const predictorStub: Predictor = {
        predict: jest.fn(async () => 'mock'),
      };
      const factoryStub = jest.spyOn(factory, 'getPredictor').mockReturnValue(predictorStub);
      await service.predict(fakeResampleResult);
      expect(rabbitService.sendToQueue).toHaveBeenCalledWith(config.JOB_STATUS_UPDATE_QUEUE, { id: 'mock', status: 'success' });
      expect(rabbitService.publish).toHaveBeenCalledWith(config.JOB_PREDICT_RESULT_EXCHANGE, {
        id: 'mock',
        result: 'mock',
        channelId: 'mock',
      });
      expect(factoryStub).toHaveBeenCalledWith('google');
      expect(configStub).toHaveBeenCalledWith('mock');
      expect(predictorStub.predict).toHaveBeenCalledWith('mock', 'mock', fakeConfig.accountData, {
        encoding: fakeResampleResult.audioFormat,
        sampleRateHertz: fakeResampleResult.sampleRate,
      });
    });

    it('should fail and send result of null to queue and failed status', async () => {
      jest.spyOn(service, 'mGetUserConfig').mockImplementation(async () => fakeConfig);
      const predictorStub: Predictor = {
        predict: jest.fn().mockRejectedValue(new Error('Fake error')),
      };
      jest.spyOn(factory, 'getPredictor').mockReturnValue(predictorStub);
      await service.predict(fakeResampleResult);
      expect(rabbitService.sendToQueue).toHaveBeenCalledWith(config.JOB_STATUS_UPDATE_QUEUE, { id: 'mock', status: 'failed' });
      expect(rabbitService.publish).toHaveBeenCalledWith(config.JOB_PREDICT_RESULT_EXCHANGE, {
        id: 'mock',
        result: null,
        channelId: 'mock',
      });
    });
  });

  describe('user config', () => {
    it('should get user config', async () => {
      const getStub = jest.spyOn(axios, 'get').mockImplementation(async () => ({
        data: 'mock',
      }));
      const projectConfig = await service.mGetUserConfig('mock');
      expect(getStub).toHaveBeenCalledWith(`${config.STORAGE_SERVICE_URL}/project/configs/mock`);
      expect(projectConfig).toEqual('mock');
    });
  });

});
