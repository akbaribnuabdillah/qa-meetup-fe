import { Module } from '@nestjs/common';
import { JobPredictionService } from './job-prediction.service';
import { PredictionFactory } from './utils/prediction/factory.prediction';

@Module({
  providers: [JobPredictionService, PredictionFactory],
})
export class JobPredictionModule {}
