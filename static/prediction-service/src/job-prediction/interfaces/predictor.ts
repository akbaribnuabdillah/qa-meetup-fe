export interface Predictor {
    predict(jobId: string, audioFile: string, account: any, predictionConfig: any): Promise<string>;
}
