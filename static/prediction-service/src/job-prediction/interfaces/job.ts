export interface JobResult {
    id: string;
    result: string;
    channelId: string;
}
