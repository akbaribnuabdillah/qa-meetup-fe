export interface IRecognitionConfig {
    languageCode: string;
    model?: string;
    alternativeLanguageCodes?: string[];
    sampleRateHertz?: number;
    encoding?: string;
    metadata?: IRecognitionMetadata;
}

export interface IRecognitionMetadata {
    interactionType: 'INTERACTION_TYPE_UNSPECIFIED' | 'DISCUSSION' | 'VOICEMAIL' | 'VOICE_SEARCH' | 'VOICE_COMMAND' | 'DICTATION';
    microphoneDistance: 'MICROPHONE_DISTANCE_UNSPECIFIED' | 'NEARFIELD' | 'MIDFIELD' | 'FARFIELD';
    originalMediaType: 'ORIGINAL_MEDIA_TYPE_UNSPECIFIED' | 'AUDIO' | 'VIDEO';
    recordingDeviceType: 'RECORDING_DEVICE_TYPE_UNSPECIFIED' | 'SMARTPHONE' | 'PC' | 'PHONE_LINE' | 'VEHICLE';
}
