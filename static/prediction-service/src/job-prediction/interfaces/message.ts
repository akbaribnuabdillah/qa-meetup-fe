export interface IncomingMessage {
    requestId: string;
    channelId: string;
    message: Message;
    partnerId: string;
    options: object;
}

export type MessageType = 'text' | 'data' | 'command';

export interface Message {
    type: MessageType;
    content?: string;
    payload?: { type?: string, url?: string };
    metadata?: object;
    id: string;
    event_time?: number;
}
