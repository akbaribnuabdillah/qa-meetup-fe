import { Module } from '@nestjs/common';
import { JobPredictionModule } from './job-prediction/job-prediction.module';
import { RabbitModule } from './rabbit/rabbit.module';

@Module({
  imports: [JobPredictionModule, RabbitModule],
})
export class AppModule {}
