export const RABBIT_CONNECTION_URL = process.env.RABBIT_CONNECTION_URL;
export const NODE_ENV = process.env.NODE_ENV;

export const IMAGER_URI = process.env.IMAGER_URI;

export const DB_HOST = process.env.DB_HOST;
export const DB_PORT = parseInt(process.env.DB_PORT, 10);
export const DB_USER = process.env.DB_USER;
export const DB_NAME = process.env.DB_NAME;
export const DB_PASSWORD = process.env.DB_PASSWORD;

export const CONFIG_CREATE_QUEUE = process.env.CONFIG_CREATE_QUEUE;
export const CONFIG_UPDATE_QUEUE = process.env.CONFIG_UPDATE_QUEUE;
export const CONFIG_DELETE_QUEUE = process.env.CONFIG_DELETE_QUEUE;
export const CONFIG_CHANNEL_CONNECT_QUEUE = process.env.CONFIG_CHANNEL_CONNECT_QUEUE;

// Departure
export const JOB_WILL_RESAMPLE_QUEUE = process.env.JOB_WILL_RESAMPLE_QUEUE;
export const JOB_WILL_PREDICT_QUEUE = process.env.JOB_WILL_PREDICT_QUEUE;
export const JOB_DELETE_QUEUE = process.env.JOB_DELETE_QUEUE;

// Arrival
export const JOB_RESAMPLE_RESULT_EXCHANGE = process.env.JOB_RESAMPLE_RESULT_EXCHANGE;
export const JOB_WILL_CREATE_QUEUE = process.env.JOB_WILL_CREATE_QUEUE;
export const JOB_STATUS_UPDATE_QUEUE = process.env.JOB_STATUS_UPDATE_QUEUE;
export const JOB_PREDICT_RESULT_EXCHANGE = process.env.JOB_PREDICT_RESULT_EXCHANGE;

export const JOB_STATUS_UPDATED_EXCHANGE = process.env.JOB_STATUS_UPDATED_EXCHANGE;

export const PORT = process.env.STORAGE_SERVICE_PORT;


export const JOB_WILL_CREATE_T2S_QUEUE = process.env.JOB_WILL_CREATE_T2S_QUEUE;
export const JOB_WILL_PREDICT_T2S_QUEUE = process.env.JOB_WILL_PREDICT_T2S_QUEUE;
export const JOB_PREDICT_T2S_RESULT_EXCHANGE = process.env.JOB_PREDICT_T2S_RESULT_EXCHANGE;
