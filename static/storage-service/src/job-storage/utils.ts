import axios from 'axios';
import * as config from '../config';
import * as fileType from 'file-type';
import urlJoin = require('url-join');
import * as FormData from 'form-data';
import { ImagerResponse } from './dto/imager.response';

export class Utils {
    static async saveToImager(id: string, fileToUpload: any): Promise<string> {
        let file: Buffer;
        // if file is a text (from speech to text)
        if (typeof fileToUpload === 'string') {
            file = Buffer.from(fileToUpload, 'base64');
        } else {
            file = fileToUpload as Buffer;
        }
        const form = new FormData();
        const fileMeta = fileType(file);
        const filename = `${id}.${fileMeta.ext}`;
        form.append('file', file, { filename });
        const url = urlJoin(config.IMAGER_URI, 'api/v1/gcp/files');
        const { data } = await axios.post<ImagerResponse>(url, form, {
            headers: {
                'Content-Type': `multipart/form-data; boundary=${form.getBoundary()}`,
            },
        });
        const { link } = data;
        return link;
    }
}
