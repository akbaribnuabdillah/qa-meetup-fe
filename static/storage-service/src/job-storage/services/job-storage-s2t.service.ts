import { Injectable, Logger } from '@nestjs/common';
import * as config from '../../config';
import { JobEntity } from '../entities/job.entity';
import { JobRepository } from '../repositories/job.repository';
import { JobCreateDto, JobResampleDto, JobStatusUpdateDto, JobPredictResultDto, JobDeleteDto } from '../dto';
import { RabbitService } from '../../rabbit/rabbit.service';
import { UserConfigChannelRepository } from '../../user-config-storage/repositories/user-config-channel.repository';
import { Utils } from '../utils';

@Injectable()
export class S2TJobStorageService {
    constructor(
        private readonly jobRepository: JobRepository,
        private readonly rabbitService: RabbitService,
        private readonly userConfigChannelRepository: UserConfigChannelRepository,
    ) {
        this.rabbitService.consume(
            { queue: config.JOB_WILL_CREATE_QUEUE, callback: this.create },
            { queue: config.JOB_STATUS_UPDATE_QUEUE, callback: this.updateStatus },
            { queue: config.JOB_DELETE_QUEUE, callback: this.delete },
        );
        this.rabbitService.subscribe(
            { exchange: config.JOB_RESAMPLE_RESULT_EXCHANGE, callback: this.saveResample},
            { exchange: config.JOB_PREDICT_RESULT_EXCHANGE, callback: this.savePrediction },
        );
    }

    /**
     * RMQ Consumer Methods
     */
    readonly create = async (job: JobCreateDto) => {
        const { audioFile, id, channelId } = job;
        const { userConfig } = await this.userConfigChannelRepository.findOneOrFail({ channelId }, { relations: ['userConfig'] });
        const jobEntity: Partial<JobEntity> = { id, configId: userConfig.id, status: 'pending' };
        this.jobRepository.insert(jobEntity)
            .then(() => {
                Logger.log(`Job ${JSON.stringify(jobEntity)} created`);
                return this.sendToResample(job, userConfig.id);
            })
            .then(() => Utils.saveToImager(id, audioFile))
            .then(location => this.jobRepository.update(id, { originalFileUrl: location }))
            .then(() => Logger.log(`Job with ID ${id} has been created`))
            .catch(err => Logger.error(err.stack));
    }

    readonly updateStatus = async (jobUpdate: JobStatusUpdateDto) => {
        const { id, status } = jobUpdate;
        this.jobRepository.update(id, { status }).then(async () => {
            Logger.log(`Job with id ${id} status updated with value ${status}`);
            this.publishStatusUpdate(id, status);
        });
    }

    readonly savePrediction = async (predictResult: JobPredictResultDto) => {
        const { id, result } = predictResult;
        this.jobRepository.update(id, { result }).then(() => {
            Logger.log(`Job with ID ${id} translated into: ${result}`);
        });
    }

    readonly delete = async (delDto: JobDeleteDto) => {
        const { id } = delDto;
        this.jobRepository.softDelete(id).then(() => {
            Logger.log(`Job with id ${id} deleted`);
        });
    }

    readonly saveResample = async (job: JobResampleDto) => {
        const { id, audioFile } = job;
        Utils.saveToImager(id, audioFile)
            .then(location => this.jobRepository.update(id, { resampledFileUrl: location }))
            .then(() => Logger.log(`Job with ID ${id} has been resampled and updated`))
            .catch(err => Logger.error(err.stack));
    }

    /**
     * Sender Methods
     */

    sendToResample(createdJob: JobCreateDto, configId: string) {
        const willResampleDto = { ...createdJob, configId };
        this.rabbitService.sendToQueue(config.JOB_WILL_RESAMPLE_QUEUE, willResampleDto);
    }

    /**
     * Publisher methods
     */
    publishStatusUpdate(jobId: string, status: string) {
        const updatedStatus = { id: jobId, status };
        this.rabbitService.publish(config.JOB_STATUS_UPDATED_EXCHANGE, updatedStatus);
    }

    /**
     * REST Handler Methods
     */
    async findOne(id: string) {
        return await this.jobRepository.findOneExceptSoftDeleted(id);
    }

    async findAll(limit: number = 10, page: number = 0) {
        return await this.jobRepository.findAllExceptSoftDeleted({ take: limit, skip: page });
    }
}
