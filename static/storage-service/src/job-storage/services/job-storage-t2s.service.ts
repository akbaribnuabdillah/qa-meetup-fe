import { Injectable, Logger } from '@nestjs/common';
import { T2SJobRepository } from '../repositories/t2s.repository';
import { RabbitService } from '../../rabbit/rabbit.service';
import * as config from '../../config';
import { T2SEntity } from '../entities/t2s.entity';
import { T2SJobCreateDto, JobT2SPredictResultDto } from '../dto';
import { UserConfigChannelRepository } from '../../user-config-storage/repositories/user-config-channel.repository';
import { Utils } from '../utils';
import { JobRepository } from '../repositories/job.repository';

@Injectable()
export class T2SJobStorageService {
    constructor(
        private readonly t2sJobRepository: T2SJobRepository,
        private readonly rabbitService: RabbitService,
        private readonly userConfigChannelRepository: UserConfigChannelRepository,
        private readonly jobRepository: JobRepository,
    ) {
        this.rabbitService.consume(
            { queue: config.JOB_WILL_CREATE_T2S_QUEUE, callback: this.t2sCreate },
        );
        this.rabbitService.subscribe(
            { exchange: config.JOB_PREDICT_T2S_RESULT_EXCHANGE, callback: this.t2sSavePrediction},
        );
    }

    readonly t2sCreate = async (job: T2SJobCreateDto) => {
        const { id, channelId } = job;
        const { userConfig } = await this.userConfigChannelRepository.findOneOrFail({ channelId }, { relations: ['userConfig'] });
        const jobEntity: Partial<T2SEntity> = { id, configId: userConfig.id, status: 'pending' };
        const predictBody = {...job, configId: userConfig.id };
        this.t2sJobRepository.insert(jobEntity)
            .then(() => Logger.log(`Job with ID ${id} has been created`))
            .then(() => this.rabbitService.sendToQueue(config.JOB_WILL_PREDICT_T2S_QUEUE, predictBody ))
            .catch(err => Logger.error(err.stack));
    }

    readonly t2sSavePrediction = async (predictResult: JobT2SPredictResultDto) => {
        const { id, result, status } = predictResult;
        if (result) {
            Utils.saveToImager(id, result)
            .then(location => this.t2sJobRepository.update(id, { status, result: location}))
            .then(() => Logger.log(`Job with ID ${id} translated to audio file`))
            .catch(err => Logger.error(err.stack));
        } else {
            this.jobRepository.update(id, { status}).then(() => {
                Logger.log(`Job with ID ${id} has failed`);
            });
        }
    }

    async findOne(id: string) {
        return this.t2sJobRepository.findOne(id);
    }

    // async findOne()
}
