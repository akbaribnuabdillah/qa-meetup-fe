import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity('t2s_jobs')
export class T2SEntity {
    @PrimaryColumn()
    id: string;

    @Column({ nullable: true})
    configId: string | null;

    @Column('enum', { enum: ['pending', 'processing', 'failed', 'success']})
    status: 'pending' | 'processing' | 'failed' | 'success';

    @Column({ nullable: true })
    result: string | null;

    @Column({ nullable: true, select: false })
    deletedAt: Date;
}
