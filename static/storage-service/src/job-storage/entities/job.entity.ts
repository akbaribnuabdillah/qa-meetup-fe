import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity('jobs')
export class JobEntity {
    @PrimaryColumn()
    id: string;

    @Column({ nullable: true})
    configId: string | null;

    @Column('enum', { enum: ['pending', 'processing', 'failed', 'success']})
    status: 'pending' | 'processing' | 'failed' | 'success';

    @Column({ nullable: true })
    result: string | null;

    @Column({ nullable: true })
    originalFileUrl: string | null;

    @Column({ nullable: true })
    resampledFileUrl: string | null;

    @Column({ nullable: true, select: false })
    deletedAt: Date;
}
