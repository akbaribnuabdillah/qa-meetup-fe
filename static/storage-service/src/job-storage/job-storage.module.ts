import { Module } from '@nestjs/common';
import { JobStorageController } from './job-storage.controller';
import { S2TJobStorageService } from './services/job-storage-s2t.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobRepository } from './repositories/job.repository';
import { UserConfigStorageModule } from '../user-config-storage/user-config-storage.module';
import { T2SJobStorageService } from './services/job-storage-t2s.service';
import { T2SJobRepository } from './repositories/t2s.repository';

@Module({
  imports: [TypeOrmModule.forFeature([JobRepository, T2SJobRepository]), UserConfigStorageModule],
  controllers: [JobStorageController],
  providers: [S2TJobStorageService, T2SJobStorageService],
})
export class JobStorageModule {}
