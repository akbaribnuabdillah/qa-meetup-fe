import { IncomingMessage } from '../interfaces';
import { Dto } from './base.dto';

export interface JobCreateDto extends Dto {
    audioFile: string;
    channelId: string;
}

export interface T2SJobCreateDto extends Dto {
    message: string;
    channelId: string;
}
