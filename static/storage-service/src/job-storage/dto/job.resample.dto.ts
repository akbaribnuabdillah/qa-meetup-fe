import { IncomingMessage } from '../interfaces';
import { Dto } from './base.dto';

export interface JobResampleDto extends Dto {
    audioFile: string;
    configId: string;
    channelId: string;
}
