import { IncomingMessage } from '../interfaces';
import { Dto } from './base.dto';
export interface JobPredictResultDto extends Dto {
    result: string;
    channelId: string;
}

export interface JobT2SPredictResultDto extends Dto {
        id: string;
        result: string;
        channelId: string;
        status: 'failed' | 'success';
}
