import { Dto } from './base.dto';

export interface JobStatusUpdateDto extends Dto {
    status: 'pending' | 'processing' | 'failed' | 'success';
}
