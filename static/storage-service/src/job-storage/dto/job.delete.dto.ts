import { Dto } from './base.dto';

export interface JobDeleteDto extends Dto {}