export interface ImagerResponse {
    name: string;
    link: string;
}
