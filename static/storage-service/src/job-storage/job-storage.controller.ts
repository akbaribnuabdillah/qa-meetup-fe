import { Controller, Param, Get, Query } from '@nestjs/common';
import { S2TJobStorageService } from './services/job-storage-s2t.service';
import { T2SJobStorageService } from './services/job-storage-t2s.service';

@Controller('jobs')
export class JobStorageController {
    constructor(
        private readonly s2tJobStorageService: S2TJobStorageService,
        private readonly t2sJobStorageService: T2SJobStorageService,
    ) {}

    @Get()
    async findAll(@Query('limit') limit: number, @Query('page') page: number) {
        return await this.s2tJobStorageService.findAll(limit, page);
    }

    @Get(':id')
    async findOne(@Param('id') id: string) {
        return await this.s2tJobStorageService.findOne(id);
    }

    @Get('t2s/:id')
    async findT2S(@Param('id') id: string) {
        return await this.t2sJobStorageService.findOne(id);
    }
}
