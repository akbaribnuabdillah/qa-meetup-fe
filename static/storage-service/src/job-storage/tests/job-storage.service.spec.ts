import { Test, TestingModule } from '@nestjs/testing';
import { S2TJobStorageService } from '../services/job-storage-s2t.service';
import { JobRepository } from '../repositories/job.repository';
import { JobEntity } from '../entities/job.entity';
import { RabbitService } from '../../rabbit/rabbit.service';
import { UserConfigChannelRepository } from '../../user-config-storage/repositories/user-config-channel.repository';
import * as config from '../../config';
import { Utils } from '../utils';
import { T2SJobRepository } from '../repositories/t2s.repository';

jest.mock('../../rabbit/rabbit.service');
jest.mock('../repositories/job.repository');

describe('JobStorageService', () => {
  let service: S2TJobStorageService;
  let mockJobRepo: JobRepository;
  let mockUserConfigChannelRepo: UserConfigChannelRepository;
  let rabbitService: RabbitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        S2TJobStorageService,
        UserConfigChannelRepository,
        T2SJobRepository,
        JobRepository,
        RabbitService,
      ],
    }).compile();
    service = module.get<S2TJobStorageService>(S2TJobStorageService);
    mockJobRepo = module.get(JobRepository);
    mockUserConfigChannelRepo = module.get(UserConfigChannelRepository);
    rabbitService = module.get(RabbitService);
  });

  it('findAll should return array of IJob', async () => {
    const result: JobEntity[] = [{
      id: '12345',
      configId: null,
      status: 'success',
      result: 'hello',
      originalFileUrl: 'S3_url',
      resampledFileUrl: 'S3_url',
      deletedAt: null,
    }];
    // fakeRepo
    const fakeFind = jest.spyOn(mockJobRepo, 'findAllExceptSoftDeleted').mockImplementation(async () => result);
    const findAll = await service.findAll();
    expect(fakeFind).toHaveBeenCalledWith({ take: 10, skip: 0 });
    expect(findAll).toEqual(result);
  });

  it('findOne should return exactly one item IJob', async () => {
    const result: JobEntity = {
      id: '12345',
      configId: null,
      status: 'success',
      result: 'hello',
      originalFileUrl: 'S3_url',
      resampledFileUrl: 'S3_url',
      deletedAt: null,
    };
    const fakeFind = jest.spyOn(mockJobRepo, 'findOneExceptSoftDeleted').mockImplementation(async () => result);
    const findOne = await service.findOne('12345');
    expect(fakeFind).toHaveBeenCalledWith('12345');
    expect(findOne).toEqual(result);
  });

  it('create should save job instance and send message to will_resample queue', async () => {
    const sendToResampleStub = jest.spyOn(service, 'sendToResample');
    const findOneStub = jest.spyOn(mockUserConfigChannelRepo, 'findOneOrFail');
    // const saveToS3Stub = jest.spyOn(service, 'mSaveAudioToS3');
    const saveToImagerStub = jest.spyOn(Utils, 'saveToImager').mockImplementation(async () => 'mock');
    const insertStub = jest.spyOn(mockJobRepo, 'insert').mockImplementation(async () => null);
    const updateStub = jest.spyOn(mockJobRepo, 'update').mockImplementation(async () => null);
    const jobEntityMock: Partial<JobEntity> = {
      id: 'mock',
      configId: 'mock',
      status: 'pending',
    };
    findOneStub.mockImplementation(async () => ({
      channelId: 'mock',
      userConfig: {
        id: 'mock',
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      },
    }));
    await service.create({
      id: 'mock',
      audioFile: 'mock',
      channelId: 'mock',
    });
    service.create({
      id: 'mock',
      audioFile: 'mock',
      channelId: 'mock',
    }).then(() => expect(findOneStub).toBeCalled())
      .then(() => expect(insertStub).toHaveBeenCalledWith(jobEntityMock))
      .then(() => expect(sendToResampleStub).toHaveBeenCalled())
      .then(() => expect(saveToImagerStub).toHaveBeenCalledWith('mock', 'mock'))
      .then(() => expect(updateStub).toHaveBeenCalledWith('mock', {
        originalFileUrl: 'mock',
      }));
  });

  it('update status should update job instance and publish message', async () => {
    const publishStatusStub = jest.spyOn(service, 'publishStatusUpdate');
    const updateStub = jest.spyOn(mockJobRepo, 'update').mockImplementation(async () => null);
    await service.updateStatus({
      id: 'mock',
      status: 'processing',
    });
    expect(publishStatusStub).toHaveBeenCalled();
    expect(updateStub).toHaveBeenCalledWith('mock', { status: 'processing' });
  });

  it('save prediction should update job with message content', async () => {
    const updateStub = jest.spyOn(mockJobRepo, 'update').mockImplementation(async () => null);
    await service.savePrediction({
      id: 'mock',
      result: 'mock',
      channelId: 'mock',
    });
    expect(updateStub).toHaveBeenCalledWith('mock', {result: 'mock'});
  });

  it('save resample should update job with resampled audio file', async () => {
    const updateStub = jest.spyOn(mockJobRepo, 'update').mockImplementation(async () => null);
    const saveToImagerStub = jest.spyOn(Utils, 'saveToImager').mockImplementation(async () => 'mock');
    service.saveResample({
      id: 'mock',
      audioFile: 'mock',
      channelId: 'mock',
      configId: 'mock',
    }).then(() => expect(saveToImagerStub).toHaveBeenCalledWith('mock', 'mock'))
      .then(() => expect(updateStub).toHaveBeenCalledWith('mock', {resampledFileUrl: 'mock'}));
  });

  it('should send to resample queue', async () => {
    const sendQueueStub = jest.spyOn(rabbitService, 'sendToQueue');
    await service.sendToResample({
      id: 'mock',
      audioFile: 'mock',
      channelId: 'mock',
    }, 'mock');
    expect(sendQueueStub).toHaveBeenCalledWith(config.JOB_WILL_RESAMPLE_QUEUE, {
      id: 'mock',
      audioFile: 'mock',
      channelId: 'mock',
      configId: 'mock',
    });
  });

  it('should publish to status updated exchange', async () => {
    const publishStub = jest.spyOn(rabbitService, 'publish');
    await service.publishStatusUpdate('mock', 'processing');
    expect(publishStub).toHaveBeenCalledWith(config.JOB_STATUS_UPDATED_EXCHANGE, { id: 'mock', status: 'processing' });
  });
});
