import { JobRepository } from '../repositories/job.repository';
import { IsNull } from 'typeorm';

describe('JobRepository', () => {
    let mockJobRepo: JobRepository;

    beforeEach(async () => {
        mockJobRepo = new JobRepository();
    });

    it('soft delete should add to deletedAt column', async () => {
        const updateStub = jest.spyOn(mockJobRepo, 'update').mockImplementation(async () => null);
        const getCurrentDate = () => new Date();
        jest.spyOn(global, 'Date').mockImplementation(() => '2019-05-14T11:01:58.135Z');
        await mockJobRepo.softDelete('mock');
        expect(updateStub).toHaveBeenCalledWith('mock', { deletedAt: getCurrentDate() });
    });

    it('findAllExceptSoftDelete should get all items where deletedAt is NULL', async () => {
        const findStub = jest.spyOn(mockJobRepo, 'find').mockImplementation(async () => null);
        await mockJobRepo.findAllExceptSoftDeleted();
        expect(findStub).toHaveBeenCalledWith({ deletedAt: IsNull() });
    });

    it('findOneExceptSoftDelete should get an item where deletedAt is NULL', async () => {
        const findStub = jest.spyOn(mockJobRepo, 'findOne').mockImplementation(async () => null);
        await mockJobRepo.findOneExceptSoftDeleted('mock');
        expect(findStub).toHaveBeenCalledWith({ id: 'mock', deletedAt: IsNull() });
    });
})