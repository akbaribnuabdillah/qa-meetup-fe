import { Test, TestingModule } from '@nestjs/testing';
import { JobStorageController } from '../job-storage.controller';
import { S2TJobStorageService } from '../services/job-storage-s2t.service';
import { JobEntity } from '../entities/job.entity';
import { JobRepository } from '../repositories/job.repository';
import { RabbitService } from '../../rabbit/rabbit.service';
import { UserConfigChannelRepository } from '../../user-config-storage/repositories/user-config-channel.repository';
import { T2SJobStorageService } from '../services/job-storage-t2s.service';
import { T2SJobRepository } from '../repositories/t2s.repository';

jest.mock('../../rabbit/rabbit.service');

describe('JobStorage Controller', () => {
  let controller: JobStorageController;
  let s2tStorageService: S2TJobStorageService;
  let t2sStorageService: T2SJobStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JobStorageController],
      providers: [
        UserConfigChannelRepository,
        JobRepository,
        T2SJobRepository,
        S2TJobStorageService,
        T2SJobStorageService,
        RabbitService,
      ],
    }).compile();

    controller = module.get<JobStorageController>(JobStorageController);
    s2tStorageService = module.get<S2TJobStorageService>(S2TJobStorageService);
    t2sStorageService = module.get(T2SJobStorageService);
  });

  describe('find', () => {
    it('findAll should return array of IJob', async () => {
      const result: JobEntity[] = [{
        id: '12345',
        configId: null,
        status: 'success',
        result: 'hello',
        originalFileUrl: 'S3_url',
        resampledFileUrl: 'S3_url',
        deletedAt: null,
      }];
      const spy = jest.spyOn(s2tStorageService, 'findAll').mockImplementation(async () => result);
      const findAll = await controller.findAll(1, 0);
      expect(spy).toHaveBeenCalled();
      expect(findAll).toEqual(result);
    });

    it('findOne should return a IJob object', async () => {
      const result: JobEntity = {
        id: '12345',
        configId: null,
        status: 'success',
        result: 'hello',
        originalFileUrl: 'S3_url',
        resampledFileUrl: 'S3_url',
        deletedAt: null,
      };
      const spy = jest.spyOn(s2tStorageService, 'findOne').mockImplementation(async () => result);
      const findAll = await controller.findOne('12345');
      expect(spy).toHaveBeenCalledWith('12345');
      expect(findAll).toEqual(result);
    });
  });
});
