import { Repository, EntityRepository, FindConditions, FindManyOptions } from 'typeorm';
import { JobEntity } from '../entities/job.entity';
import { IsNull } from 'typeorm';

@EntityRepository(JobEntity)
export class JobRepository extends Repository<JobEntity> {

    async softDelete(id: string) {
        try {
            this.update(id, { deletedAt: new Date() });
            return id;
        } catch (err) {
            throw err;
        }
    }

    findAllExceptSoftDeleted(options?: FindConditions<JobEntity> & FindManyOptions<JobEntity>) {
        return this.find({ ...options, deletedAt: IsNull() });
    }

    findOneExceptSoftDeleted(id: string) {
        return this.findOne({ id, deletedAt: IsNull() });
    }
}
