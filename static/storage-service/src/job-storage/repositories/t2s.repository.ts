import { Repository, EntityRepository, FindConditions, FindManyOptions } from 'typeorm';
import { T2SEntity } from '../entities/t2s.entity';
import { IsNull } from 'typeorm';

@EntityRepository(T2SEntity)
export class T2SJobRepository extends Repository<T2SEntity> {

    async softDelete(id: string) {
        try {
            this.update(id, { deletedAt: new Date() });
            return id;
        } catch (err) {
            throw err;
        }
    }

    findAllExceptSoftDeleted(options?: FindConditions<T2SEntity> & FindManyOptions<T2SEntity>) {
        return this.find({ ...options, deletedAt: IsNull() });
    }

    findOneExceptSoftDeleted(id: string) {
        return this.findOne({ id, deletedAt: IsNull() });
    }
}
