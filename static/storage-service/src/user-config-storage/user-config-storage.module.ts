import { Module } from '@nestjs/common';
import { UserConfigStorageController } from './user-config-storage.controller';
import { UserConfigStorageService } from './user-config-storage.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserConfigRepository } from './repositories/user-config.repository';
import { UserConfigChannelRepository } from './repositories/user-config-channel.repository';

@Module({
  controllers: [UserConfigStorageController],
  providers: [UserConfigStorageService],
  imports: [TypeOrmModule.forFeature([UserConfigRepository, UserConfigChannelRepository])],
  exports: [TypeOrmModule.forFeature([UserConfigChannelRepository])],
})
export class UserConfigStorageModule {}
