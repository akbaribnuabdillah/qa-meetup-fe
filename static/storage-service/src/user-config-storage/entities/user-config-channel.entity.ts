import { Entity, PrimaryColumn, Column, ManyToOne } from 'typeorm';
import { UserConfigEntity } from './user-config.entity';

@Entity('user_config_channel')
export class UserConfigChannelEntity {
    @PrimaryColumn()
    channelId: string;

    @ManyToOne(() => UserConfigEntity, userConfig => userConfig.channels, { nullable: false })
    userConfig: UserConfigEntity;
}
