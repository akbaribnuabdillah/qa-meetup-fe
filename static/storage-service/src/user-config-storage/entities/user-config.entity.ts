import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { IBahasaKitaAccount, IGoogleServiceAccount } from '../interfaces/account';
import { UserConfigChannelEntity } from './user-config-channel.entity';

@Entity('user_config')
export class UserConfigEntity {
    @PrimaryColumn()
    id: string;

    @Column()
    projectId: string;

    @Column('enum', { enum: ['google', 'bahasakita'] })
    provider: 'google' | 'bahasakita';

    @Column('json')
    accountData: IBahasaKitaAccount | IGoogleServiceAccount;

    @OneToMany(() => UserConfigChannelEntity, channel => channel.userConfig, { nullable: true })
    channels?: UserConfigChannelEntity[];
}
