import { Injectable, Logger } from '@nestjs/common';
import { UserConfigRepository } from './repositories/user-config.repository';
import { RabbitService } from '../rabbit/rabbit.service';
import * as amqp from 'amqplib';
import { IUserConfigDto } from './dto/user-config.dto';
import { UserConfigEntity } from './entities/user-config.entity';
import { IUserConfigDeleteDto } from './dto/user-config.delete.dto';
import * as configFile from '../config';
import { FindOneOptions, FindManyOptions } from 'typeorm';
import { UserConfigChannelRepository } from './repositories/user-config-channel.repository';

@Injectable()
export class UserConfigStorageService {
    constructor(
        private readonly userConfigRepository: UserConfigRepository,
        private readonly amqpConnectionService: RabbitService,
        private readonly userConfigChannelRepository: UserConfigChannelRepository,
    ) {
        amqpConnectionService.consume(
            { queue: configFile.CONFIG_CREATE_QUEUE, callback: this.create },
            { queue: configFile.CONFIG_UPDATE_QUEUE, callback: this.update },
            { queue: configFile.CONFIG_DELETE_QUEUE, callback: this.delete },
            { queue: configFile.CONFIG_CHANNEL_CONNECT_QUEUE, callback: this.connectConfigToChannel },
        );
    }

    readonly create = async (userConfig: IUserConfigDto) => {
        this.userConfigRepository.insert(userConfig as UserConfigEntity).then(() => {
            Logger.log(`User config ${userConfig.id} has been created`);
        });
    }

    readonly update = async (updatedUserConfigMessage: IUserConfigDto) => {
        const { id, ...rest } = updatedUserConfigMessage;
        this.userConfigRepository.update(id, rest).then(() => {
            Logger.log(`User config with ID ${updatedUserConfigMessage.id} with \
                        user ID ${updatedUserConfigMessage.projectId} has been updated`);
        });
    }

    readonly delete = async (deletedUserConfigMessage: IUserConfigDeleteDto) => {
        this.userConfigRepository.delete(deletedUserConfigMessage).then(() => {
            Logger.log(`User config with ID ${deletedUserConfigMessage.id} has been deleted`);
        });
    }

    readonly connectConfigToChannel = async (ids: { id: string, channelId: string }) => {
        const { id, channelId } = ids;
        this.userConfigRepository.findOneOrFail(id)
        .then(async userConfig => {
            this.userConfigChannelRepository.save({
                channelId,
                userConfig,
            }).then(() => {
                Logger.log(`User Config ${id} with project ID ${userConfig.projectId} connected to channel ${channelId}`);
            });
        })
        .catch(err => Logger.error(err));
    }

    async findOne(configId: string) {
        const options: FindOneOptions = {
            relations: ['channels'],
        };
        return await this.userConfigRepository.findOne({ id: configId, ...options });
    }

    async findByProjectId(projectId: string, limit: number = 10, page: number = 0) {
        const options: FindManyOptions = {
            take: limit,
            skip: page,
            relations: ['channels'],
            where: {
                projectId,
            },
        };
        return await this.userConfigRepository.find(options);
    }

}
