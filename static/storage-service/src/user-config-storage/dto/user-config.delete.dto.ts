import { IsString } from 'class-validator';

export interface IUserConfigDeleteDto {
    id: string;
}
