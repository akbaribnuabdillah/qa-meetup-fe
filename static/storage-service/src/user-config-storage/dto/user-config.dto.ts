import { IBahasaKitaAccount, IGoogleServiceAccount } from '../interfaces/account';

export interface IUserConfigDto {
    id: string;
    projectId: string;
    provider: 'google' | 'bahasakita';
    accountData: IGoogleServiceAccount | IBahasaKitaAccount;
}
