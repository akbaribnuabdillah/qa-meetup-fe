import { Controller, Get, Param, Query } from '@nestjs/common';
import { UserConfigStorageService } from './user-config-storage.service';

@Controller('project')
export class UserConfigStorageController {
    constructor(
        private readonly userConfigStorageService: UserConfigStorageService,
    ) {}

    @Get(':id/configs')
    async findByUserId(@Param('id') id: string, @Query('limit') limit: number, @Query('page') page: number) {
        return await this.userConfigStorageService.findByProjectId(id, limit, page);
    }

    @Get('/configs/:id')
    async findOne(@Param('id') configId: string) {
        return await this.userConfigStorageService.findOne(configId);
    }
}
