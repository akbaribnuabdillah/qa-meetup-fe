import { Test, TestingModule } from '@nestjs/testing';
import { UserConfigStorageService } from '../user-config-storage.service';
import { UserConfigRepository } from '../repositories/user-config.repository';
import { RabbitService } from '../../rabbit/rabbit.service';
import { UserConfigChannelRepository } from '../repositories/user-config-channel.repository';

jest.mock('../../rabbit/rabbit.service');
jest.mock('../repositories/user-config.repository');
jest.mock('../repositories/user-config-channel.repository');

describe('UserConfigStorageService', () => {
  let service: UserConfigStorageService;
  let mockUserConfigRepo: UserConfigRepository;
  let mockUserConfigChannelRepo: UserConfigChannelRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserConfigStorageService,
        RabbitService,
        UserConfigRepository,
        UserConfigChannelRepository,
      ],
    }).compile();

    service = module.get<UserConfigStorageService>(UserConfigStorageService);
    mockUserConfigRepo = module.get(UserConfigRepository);
    mockUserConfigChannelRepo = module.get(UserConfigChannelRepository);
  });

  describe('find', () => {
    it('findByProjectId should return array of UserConfigEntity with user id 34567', async () => {
      const result: any = [{
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_credentials: 'asdiasdjsaidj123',
          entity: 'kata',
        },
      }];
      const mockFind = jest.spyOn(mockUserConfigRepo, 'find').mockReturnValue(result);
      const findByUserId = await service.findByProjectId('34567', 2, 0);
      expect(mockFind).toHaveBeenCalledWith({
        relations: ['channels'],
        take: 2,
        skip: 0,
        where: {
          projectId: '34567',
        },
      });
      expect(findByUserId).toEqual(result);
    });

    it('findOne should return a UserConfigEntity with id 12345', async () => {
      const result: any = {
        id: '12345',
        userId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_credentials: 'asdiasdjsaidj123',
          entity: 'kata',
        },
      };
      const mockFind = jest.spyOn(mockUserConfigRepo, 'findOne').mockReturnValue(result);
      const findOne = await service.findOne('12345');
      expect(mockFind).toHaveBeenCalledWith({id: '12345', relations: ['channels']});
      expect(findOne).toEqual(result);
    });
  });

  describe('CUD', () => {
    it('create should insert user config to DB', async () => {
      const insertStub = jest.spyOn(mockUserConfigRepo, 'insert').mockImplementation(async () => null);
      await service.create({
        id: 'mock',
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      });
      expect(insertStub).toHaveBeenCalledWith({
        id: 'mock',
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      });
    });

    it('update should update user config in DB', async () => {
      const updateStub = jest.spyOn(mockUserConfigRepo, 'update').mockImplementation(async () => null);
      await service.update({
        id: 'mock',
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      });
      expect(updateStub).toHaveBeenCalledWith('mock', {
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      });
    });

    it('delete should delete user config from DB', async () => {
      const deleteStub = jest.spyOn(mockUserConfigRepo, 'delete').mockImplementation(async () => null);
      await service.delete({ id: 'mock' });
      expect(deleteStub).toHaveBeenCalledWith({ id: 'mock' });
    });

    it('connect channel to config should create a row in UserConfigChannel table', async () => {
      const findConfigObjStub = jest.spyOn(mockUserConfigRepo, 'findOneOrFail').mockImplementation(async () => ({
        id: 'mock',
        projectId: 'mock',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'mock',
          entity: 'mock',
        },
      }));
      const userConfigChannelUpsertStub = jest.spyOn(mockUserConfigChannelRepo, 'save').mockImplementation(async () => null);
      await service.connectConfigToChannel({ id: 'mock', channelId: 'mock' });
      expect(findConfigObjStub).toHaveBeenCalledWith('mock');
      expect(userConfigChannelUpsertStub).toHaveBeenCalledWith({
        channelId: 'mock',
        userConfig: {
          id: 'mock',
          projectId: 'mock',
          provider: 'bahasakita',
          accountData: {
            base_64_encoded_credential: 'mock',
            entity: 'mock',
          },
        },
      });
    });
  });
});
