import { Test, TestingModule } from '@nestjs/testing';
import { UserConfigStorageController } from '../user-config-storage.controller';
import { UserConfigStorageService } from '../user-config-storage.service';
import { UserConfigRepository } from '../repositories/user-config.repository';
import { RabbitService } from '../../rabbit/rabbit.service';
import { UserConfigEntity } from '../entities/user-config.entity';
import { UserConfigChannelRepository } from '../repositories/user-config-channel.repository';

jest.mock('../../rabbit/rabbit.service');

describe('UserConfigStorage Controller', () => {
  let controller: UserConfigStorageController;
  let service: UserConfigStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserConfigStorageController],
      providers: [
        UserConfigStorageService,
        RabbitService,
        UserConfigRepository,
        UserConfigChannelRepository,
      ],
    }).compile();

    controller = module.get<UserConfigStorageController>(UserConfigStorageController);
    service = module.get(UserConfigStorageService);
  });

  describe('find', () => {
    it('findOne should return a UserConfigEntity', async () => {
      const result: UserConfigEntity = {
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'asdiasdjsaidj123',
          entity: 'kata',
        },
      };
      const spy = jest.spyOn(service, 'findOne').mockImplementation(async () => result);
      const findOne = await controller.findOne('12345');
      expect(spy).toHaveBeenCalledWith('12345');
      expect(findOne).toEqual(result);
    });

    it('findByUserId should return array of UserConfigEntity with user ID 34567', async () => {
      const result: UserConfigEntity[] = [{
        id: '12345',
        projectId: '34567',
        provider: 'bahasakita',
        accountData: {
          base_64_encoded_credential: 'asdiasdjsaidj123',
          entity: 'kata',
        },
      }];
      const spy = jest.spyOn(service, 'findByProjectId').mockImplementation(async () => result);
      const findByUserId = await controller.findByUserId('34567', 2, 0);
      expect(spy).toHaveBeenCalledWith('34567', 2, 0);
      expect(findByUserId).toEqual(result);
    });
  });
});
