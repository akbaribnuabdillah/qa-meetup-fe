import { Repository, EntityRepository } from 'typeorm';
import { UserConfigEntity } from '../entities/user-config.entity';

@EntityRepository(UserConfigEntity)
export class UserConfigRepository extends Repository<UserConfigEntity> {}
