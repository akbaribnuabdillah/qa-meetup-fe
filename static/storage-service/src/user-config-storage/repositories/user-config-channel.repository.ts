import { Repository, EntityRepository } from 'typeorm';
import { UserConfigChannelEntity } from '../entities/user-config-channel.entity';

@EntityRepository(UserConfigChannelEntity)
export class UserConfigChannelRepository extends Repository<UserConfigChannelEntity> {}
