import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PORT } from './config';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: true });
  app.setGlobalPrefix('storage');
  app.enableCors();
  Logger.log(`Storage Service starts at port ${PORT}`);
  await app.listen(PORT);
}
bootstrap();
