import { Module, Logger } from '@nestjs/common';
import { UserConfigStorageModule } from './user-config-storage/user-config-storage.module';
import * as configFile from './config';
import { JobStorageModule } from './job-storage/job-storage.module';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { RabbitModule } from './rabbit/rabbit.module';
import { createConnection } from 'typeorm';

@Module({
  imports: [
    RabbitModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: configFile.DB_HOST,
      port: configFile.DB_PORT,
      username: configFile.DB_USER,
      database: configFile.DB_NAME,
      password: configFile.DB_PASSWORD,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: configFile.NODE_ENV !== 'production',
      logging: ['error'],
    }),
    UserConfigStorageModule,
    JobStorageModule,
  ],
})
export class AppModule {
  constructor() {
    createConnection({
      type: 'mysql',
      host: configFile.DB_HOST,
      port: configFile.DB_PORT,
      username: configFile.DB_USER,
      database: configFile.DB_NAME,
      password: configFile.DB_PASSWORD,
      logging: ['error'],
    }).catch(err => Logger.error(err.message));
  }
}
