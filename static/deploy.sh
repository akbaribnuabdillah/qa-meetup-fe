#!/bin/bash
history=$(git diff --name-only HEAD^1)
TRUNC_COMMIT=$(git rev-list --no-walk HEAD)
rootFolder=$(pwd)
builtSvcArray=()

# somehow bash can split multiline input
for i in $history;
do
    SVC_NAME=$(echo $i | sed 's/^\([^\/]*\)\/.*$/\1/g')
    if [[ "${SVC_NAME}" == "deploy.sh" || "${SVC_NAME}" == "bitbucket-pipelines.yml" ]]; then
        continue;
    fi

    if [[ ! " ${builtSvcArray[@]} " =~ " ${SVC_NAME} " ]]; then
        # get service name

        IMG_NAME=$(echo $SVC_NAME | sed 's/-service//g')
        IMAGE_NAME="speech-to-text-"$IMG_NAME

        cd $rootFolder/$SVC_NAME
        cp docker/kubernetes-gcp.yml .
        sh docker/env-staging.sh

        docker login --username $DOCKER_USERNAME --password $DOCKER_PASSWORD
        docker build -t=$IMAGE_NAME .
        docker tag $IMAGE_NAME $DOCKER_TEAMNAME/$IMAGE_NAME:$TRUNC_COMMIT
        docker push $DOCKER_TEAMNAME/$IMAGE_NAME:$TRUNC_COMMIT

        cp -R $rootFolder/ci-scripts ci-scripts
        sh ci-scripts/scripts/pipelines-gcp-staging.sh
    fi
    builtSvcArray+=("$SVC_NAME")
done

