import axios from 'axios';

const API_URL = process.env.GATSBY_EMPFANG_URL;

export async function register(username: string, email: string, accountType: string, company: string, verify: string) {
  try {
    const resp = await axios({
      method: 'post',
      baseURL: API_URL,
      url: '/api/v1/signup',
      data: {
        username,
        email,
        'g-recaptcha-response': verify,
        profile: {
          entityName: company,
          type: accountType,
        },
      },
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
    });
    const data = resp.data;
    return data;
  } catch (error) {
    throw error;
  }
}

export async function checkToken(id: string) {
  try {
    const resp = await axios({
      method: 'get',
      baseURL: API_URL,
      url: `/api/v1/confirm/${id}`,
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
    });
    const data = resp.data;
    return data;
  } catch (error) {
    throw error;
  }
}

export async function storePassword(password: string, userId: string) {
  try {
    const resp = await axios({
      method: 'post',
      baseURL: API_URL,
      url: '/api/v1/create-password',
      data: {
        password,
        id: userId,
      },
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
    });
    const data = resp.data;
    return data;
  } catch (error) {
    throw error;
  }
}

export async function checkInviteToken(id: string) {
  try {
    const resp = await axios({
      method: 'post',
      baseURL: API_URL,
      url: `/api/v1/confirm/${id}`,
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
    });
    const data = resp.data;
    return data;
  } catch (error) {
    throw error;
  }
}

export async function storeInviteUser(password: string, userId: string, username: string) {
  try {
    const resp = await axios({
      method: 'post',
      baseURL: API_URL,
      url: '/api/v1/create-password-cms',
      data: {
        username,
        password,
        id: userId,
      },
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
    });
    const data = resp.data;
    return data;
  } catch (error) {
    throw error;
  }
}
