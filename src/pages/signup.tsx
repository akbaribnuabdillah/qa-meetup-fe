import * as React from 'react';

import { IndexLayout } from '../layouts/index';
import {
  AuthWrapperInner,
  AuthContainer,
  AuthWrapperLogo,
  AuthWrapperFooter,
  AuthContent,
  AuthFooter,
} from '../components/auth/AuthWrapper';
import { ConfirmUserWrapper } from '../components/auth/confirm-user';
import { SignupForm, SignupSuccess } from '../components/auth/signup';

import Logo from '../assets/images/logo-platform.png';

const WEB_URL = process.env.GATSBY_WEB_URL;
const GATSBY_PLATFORM_URL = process.env.GATSBY_PLATFORM_URL;

interface SignupPageState {
  signupComplete: boolean;
}

class SignupPage extends React.Component<{}, SignupPageState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      signupComplete: false,
    };
  }

  public componentDidMount() {
    if (typeof window !== 'undefined' && window.gtag) {
      window.gtag('event', 'conversion', {
        send_to: `${process.env.GATSBY_GOOGLE_GTAG_ID}/${process.env.GATSBY_GOOGLE_AW_CONVERSION_LABEL}`,
      });
    }
  }

  public render() {
    const { signupComplete } = this.state;

    return (
      <IndexLayout>
        <ConfirmUserWrapper>
          <AuthWrapperInner>
            <AuthContainer>
              <AuthContent>
                <AuthWrapperLogo className="mb-5">
                  <img src={Logo} alt="Kata Platform" />
                </AuthWrapperLogo>
                {signupComplete ? (
                  <SignupSuccess />
                ) : (
                  <SignupForm
                    onSuccess={_ => {
                      this.setState({
                        signupComplete: true,
                      });
                    }}
                  />
                )}
              </AuthContent>
              <AuthFooter>
                <p>
                  Already have an account? <a href={`${GATSBY_PLATFORM_URL}/login`}>Login</a>
                </p>
              </AuthFooter>
            </AuthContainer>
          </AuthWrapperInner>
          <AuthWrapperFooter>
            <p>
              Presented by <a href={WEB_URL}>Kata.ai</a>
            </p>
          </AuthWrapperFooter>
        </ConfirmUserWrapper>
      </IndexLayout>
    );
  }
}

export default SignupPage;
