import * as React from 'react';
import { Router, RouteComponentProps } from '@reach/router';

import { ConfirmUserFormContainer, ConfirmUserWrapper } from '../components/auth/confirm-user';
import { IndexLayout } from '../layouts';

interface SignupPageState {
  signupComplete: boolean;
}

class ConfirmPage extends React.Component<RouteComponentProps<{}>, SignupPageState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      signupComplete: false,
    };
  }

  public render() {
    return (
      <IndexLayout>
        <ConfirmUserWrapper>
          <Router>
            <ConfirmUserFormContainer path="/confirm/:token" />
          </Router>
        </ConfirmUserWrapper>
      </IndexLayout>
    );
  }
}

export default ConfirmPage;
