import * as React from 'react';
import { Router, RouteComponentProps } from '@reach/router';

import { ConfirmClientFormContainer, ConfirmClientWrapper } from '../components/auth/invite';
import { IndexLayout } from '../layouts';

interface SignupPageState {
  signupComplete: boolean;
}

class Invite extends React.Component<RouteComponentProps<{}>, SignupPageState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      signupComplete: false,
    };
  }

  public render() {
    return (
      <IndexLayout>
        <ConfirmClientWrapper>
          <Router>
            <ConfirmClientFormContainer path="/invite/:token" />
          </Router>
        </ConfirmClientWrapper>
      </IndexLayout>
    );
  }
}

export default Invite;
