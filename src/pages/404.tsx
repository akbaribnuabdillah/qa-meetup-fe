import * as React from 'react';
import { Link } from 'gatsby';

import ErrorLayout from '../layouts/error';
import { ErrorMessage } from '../components/ui/ErrorMessage';
import NotFoundImage from '../assets/images/errors/404.svg';

const NotFoundPage = () => (
  <ErrorLayout>
    <ErrorMessage image={NotFoundImage} title="Page is not available">
      Apologies for the inconvenience, the page you are looking for does not exist. Maybe start from the{' '}
      <Link to="/">home page</Link>.
    </ErrorMessage>
  </ErrorLayout>
);

export default NotFoundPage;
