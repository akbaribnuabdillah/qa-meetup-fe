import React from 'react';
import { withPrefix } from 'gatsby';

interface HTMLProps {
  body: string;
  htmlAttributes: Record<string, any>;
  bodyAttributes: Record<string, any>;
  preBodyComponents: React.ReactNodeArray;
  postBodyComponents: React.ReactNodeArray;
  headComponents: React.ReactNodeArray;
}

const CONVERSION_ID = process.env.GATSBY_GOOGLE_AW_CONVERSION_ID;
const CONVERSION_LABEL = process.env.GATSBY_GOOGLE_AW_CONVERSION_LABEL;

export default class HTML extends React.Component<HTMLProps> {
  render() {
    return (
      <html {...this.props.htmlAttributes}>
        <head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <link rel="icon" type="image/png" sizes="32x32" href={withPrefix('/favicon-32x32.png')} />
          <link rel="icon" type="image/png" sizes="16x16" href={withPrefix('/favicon-16x16.png')} />
          <link rel="shortcut icon" href={withPrefix('/favicon.ico')} />
          <link rel="apple-touch-icon" sizes="180x180" href={withPrefix('/apple-touch-icon.png')} />
          {this.props.headComponents}
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: `/* <![CDATA[ */
            var google_conversion_id = ${CONVERSION_ID};
            var google_conversion_label = '${CONVERSION_LABEL}';
            var google_remarketing_only = false;
            /* ]]> */`,
            }}
          />
          <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js" />
        </head>
        <body {...this.props.bodyAttributes}>
          {this.props.preBodyComponents}
          <noscript key="noscript" id="gatsby-noscript">
            This app works best with JavaScript enabled.
          </noscript>
          <noscript>
            <div style={{ display: 'inline' }}>
              <img
                height="1"
                width="1"
                style={{ borderStyle: 'none' }}
                alt=""
                src="https://www.googleadservices.com/pagead/conversion/810115091/?label=aOeKCLnBuIgBEJPApYID&amp;guid=ON&amp;script=0"
              />
            </div>
          </noscript>
          <div key={`body`} id="___gatsby" dangerouslySetInnerHTML={{ __html: this.props.body }} />
          {this.props.postBodyComponents}
        </body>
      </html>
    );
  }
}
