export interface SignupFormValues {
  username: string;
  email: string;
  account_type: string;
  company: string;
  recaptcha: string;
}

export interface ConfirmUserFormValues {
  password: string;
  confirm_password: string;
}

export interface ConfirmClientFormValues {
  username: string;
  password: string;
  confirm_password: string;
}
