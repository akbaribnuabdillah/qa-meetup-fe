import * as React from 'react';
import styled from 'styled-components';
import Helmet from 'react-helmet';
import { StaticQuery, graphql } from 'gatsby';

import '@kata-kit/fonts/museo-sans-rounded.css';
import '@kata-kit/fonts/kata-icons.css';

import { EmptyMessage } from '@kata-kit/common';
import { Wrapper } from '@kata-kit/layout';
import { KataReset } from '@kata-kit/reset';

import Logo from '../assets/images/logo-platform.png';
import MaintenanceImage from '../assets/images/maintenance.svg';
import { variables } from '@kata-kit/theme';

type StaticQueryProps = {
  site: {
    siteMetadata: {
      title: string;
      description: string;
      keywords: string;
      gitTag: string;
      gitRevision: string;
    };
  };
};

const style = {
  paddingTop: '48px',
};

const MobileWrapper = styled('div')`
  padding: 24px 40px;

  @media (min-width: ${variables.breakpoints.sm}px) {
    display: none;
  }
`;

const MobileLogo = styled('img')`
  height: 32px;
`;

const NonMobileWrapper = styled(Wrapper)`
  @media (max-width: ${variables.breakpoints.sm - 1}px) {
    display: none;
  }
`;

export const IndexLayout: React.SFC = ({ children }) => (
  <StaticQuery
    query={graphql`
      query IndexLayoutQuery {
        site {
          siteMetadata {
            title
            description
            keywords
            gitTag
            gitRevision
          }
        }
      }
    `}
  >
    {(data: StaticQueryProps) => (
      <KataReset>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: data.site.siteMetadata.description },
            { name: 'keywords', content: data.site.siteMetadata.keywords },
            { name: 'version', content: data.site.siteMetadata.gitTag },
            { name: 'revision', content: data.site.siteMetadata.gitRevision },
          ]}
        />
        <MobileWrapper>
          <div className="text-left mr-2">
            <MobileLogo src={Logo} className="maintenance-logo" alt="Kata Platform" />
          </div>
          <div className="maintenance-board" style={style}>
            <EmptyMessage image={MaintenanceImage} title="Device Unsupported">
              To use Kata | Platform, you need a desktop with a minimum screen width of 1024 pixels. We are sorry for
              the inconvenience.
              <br />
              Need help? Please reach out to{' '}
              <a href="mailto:support@kata.ai">
                <strong>support@kata.ai</strong>
              </a>
            </EmptyMessage>
          </div>
        </MobileWrapper>
        <NonMobileWrapper>{children}</NonMobileWrapper>
      </KataReset>
    )}
  </StaticQuery>
);
