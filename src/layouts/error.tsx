import React from 'react';
import styled from 'styled-components';

import { variables } from '@kata-kit/theme';

import { IndexLayout } from '.';

const ErrorLayout: React.SFC = ({ children }) => (
  <IndexLayout>
    <Root>{children}</Root>
  </IndexLayout>
);

export default ErrorLayout;

const Root = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
  width: 100%;
  background-color: ${variables.colors.neutral02};
  text-align: center;
`;
