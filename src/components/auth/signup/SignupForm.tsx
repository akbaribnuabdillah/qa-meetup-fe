import * as React from 'react';
import { FormikProps, Form, Formik, FastField, FieldProps, Field } from 'formik';
import get from 'lodash-es/get';
import * as yup from 'yup';
import ReCAPTCHA from 'react-google-recaptcha';

import { Button } from '@kata-kit/button';
import { Banner } from '@kata-kit/banner';
import { FormGroup, InputGroup, InputAddon, InputText, FormError } from '@kata-kit/form';

import { SignupFormValues } from '../../../interfaces';
import { Select } from '../../ui/Select';
import { register } from '../../../utils/api';

interface SignupFormProps {
  onSuccess?: (response: SignupFormValues) => void;
}

const RECAPTCHA_SECRET = process.env.GATSBY_RECAPTCHA_SECRET;

const recaptchaRef = React.createRef<any>();

const initialValues: SignupFormValues = {
  username: '',
  email: '',
  account_type: '',
  company: '',
  recaptcha: '',
};

const validationSchema = yup.object().shape({
  username: yup
    .string()
    .matches(
      /^[a-zA-Z0-9_]+$/, // allow letters, numbers, and underscores
      'Please enter a valid username. A username should only contain alphanumeric characters, hyphens and underscores (no spaces).'
    )
    .min(5, 'Username must be 5-15 characters long.')
    .max(15, 'Username must be 5-15 characters long.')
    .required('Username is required.'),
  email: yup
    .string()
    .email('Invalid email format.')
    .required('Email is required.'),
  account_type: yup.string().required('Account type is required.'),
  company: yup.string().required('Company/School name is required.'),
  recaptcha: yup.string().nullable(true), // .required('ReCAPTCHA challenge must be completed.'),
});

const InnerForm = ({
  values,
  isSubmitting,
  isValid,
  setFieldValue,
  setFieldTouched,
  touched,
  status,
  errors,
}: FormikProps<SignupFormValues>) => (
  <Form>
    {!!status && (
      <FormGroup>
        <Banner state="error" message={status} />
      </FormGroup>
    )}
    <FastField name="username">
      {({ field, form }: FieldProps<SignupFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-account" />
            </InputAddon>
            <InputText
              type="text"
              placeholder="Username"
              autoFocus
              addon
              {...field}
              errors={!!form.errors.username && form.touched.username}
            />
          </InputGroup>
          {!!form.errors.username && form.touched.username && <FormError>{form.errors.username}</FormError>}
        </FormGroup>
      )}
    </FastField>
    <FastField name="email">
      {({ field, form }: FieldProps<SignupFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-email" />
            </InputAddon>
            <InputText
              type="email"
              placeholder="Email address"
              addon
              {...field}
              errors={!!form.errors.email && form.touched.email}
            />
          </InputGroup>
          {!!form.errors.email && form.touched.email && <FormError>{form.errors.email}</FormError>}
        </FormGroup>
      )}
    </FastField>
    <FastField name="account_type">
      {({ field, form }: FieldProps<SignupFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-account-type" />
            </InputAddon>
            <Select placeholder="Select account type..." {...field} addon>
              <option value="" disabled>
                Select account type...
              </option>
              <option value="developers">Developers - Professional</option>
              <option value="student">Developers - Student</option>
              {/* <option value="enterprise">Enterprise</option> */}
            </Select>
          </InputGroup>
          {!!form.errors.account_type && form.touched.account_type && <FormError>{form.errors.account_type}</FormError>}
        </FormGroup>
      )}
    </FastField>
    {!!values.account_type && (
      <Field name="company">
        {({ field, form }: FieldProps<SignupFormValues>) => (
          <FormGroup>
            <InputGroup>
              <InputAddon isIcon>
                <i className="icon-building" />
              </InputAddon>
              <InputText
                type="text"
                placeholder={values.account_type === 'student' ? 'School' : 'Company'}
                addon
                {...field}
                errors={!!form.errors.company && form.touched.company}
              />
            </InputGroup>
            {!!form.errors.company && form.touched.company && <FormError>{form.errors.company}</FormError>}
          </FormGroup>
        )}
      </Field>
    )}
    <FormGroup>
      {RECAPTCHA_SECRET && (
        <>
          <ReCAPTCHA
            ref={recaptchaRef}
            sitekey={RECAPTCHA_SECRET}
            onChange={token => {
              setFieldValue('recaptcha', token);
              setFieldTouched('recaptcha', true);
            }}
          />
          {!!errors.recaptcha && touched.recaptcha && <FormError>{errors.recaptcha}</FormError>}
        </>
      )}
    </FormGroup>
    <div className="mt-3">
      <Button type="submit" color="primary" block disabled={!isValid} loading={isSubmitting}>
        Sign Up
      </Button>
    </div>
  </Form>
);

export const SignupForm: React.FC<SignupFormProps> = ({ onSuccess }) => (
  <Formik
    validationSchema={validationSchema}
    initialValues={initialValues}
    onSubmit={async (values, { setSubmitting, setStatus }) => {
      const { username, email, account_type, company, recaptcha } = values;
      setSubmitting(true);

      try {
        const response = await register(username, email, account_type, company, recaptcha);

        if (onSuccess) {
          onSuccess(response);
        }
      } catch (err) {
        const errorMessage = get(err, 'response.data.message', 'Unknown error');
        setStatus(`Failed to sign up: ${errorMessage}`);
      } finally {
        setSubmitting(false);
      }
    }}
  >
    {InnerForm}
  </Formik>
);
