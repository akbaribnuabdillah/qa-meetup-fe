import * as React from 'react';
import styled from 'styled-components';

import RegisterComplete from '../../../assets/images/happy-bot.svg';

const CenterDiv = styled('div')`
  text-align: center;
`;

export const SignupSuccess: React.FC = () => (
  <>
    <CenterDiv className="mt-3">
      <img src={RegisterComplete} className="auth-img" alt="Thanks For Signing Up" />
    </CenterDiv>
    <CenterDiv className="mt-3">
      <h4 className="text-success">Thanks For Signing Up</h4>
      <p className="paragraph">
        Please check your email to find the confirmation link. Meanwhile, check out our documentation on{' '}
        <a href="https://docs.kata.ai" target="_blank" rel="noopener noreferrer">
          docs.kata.ai
        </a>
        .
      </p>
    </CenterDiv>
  </>
);
