import * as React from 'react';
import * as Yup from 'yup';
import get from 'lodash-es/get';
import { Formik, FormikProps, Form, FastField, FieldProps } from 'formik';

import { Banner } from '@kata-kit/banner';
import { Button } from '@kata-kit/button';
import { FormGroup, InputGroup, InputAddon, InputText, FormError } from '@kata-kit/form';

import { ConfirmUserFormValues } from '../../../interfaces';
import { CenterText } from '../../ui/CenterText';
import { storePassword } from '../../../utils/api';

const initialValues: ConfirmUserFormValues = {
  password: '',
  confirm_password: '',
};

const validationSchema = Yup.object({
  password: Yup.string()
    .min(8, 'Password must be at least 8 characters.')
    .required('Please enter a password.'),
  confirm_password: Yup.string()
    .min(8, 'Password must be at least 8 characters.')
    .oneOf([Yup.ref('password')], `Passwords don't match.`)
    .required('Please enter your password again.'),
});

interface ConfirmUserFormProps {
  userId: string;
  onSuccess?: (response: ConfirmUserFormValues) => void;
}

const InnerForm = ({ isSubmitting, isValid, status }: FormikProps<ConfirmUserFormValues>) => (
  <Form>
    <CenterText>
      <h4>Create a password</h4>
      <p>Welcome to Kata Platform! To get started, please enter a password for your account.</p>
    </CenterText>
    {!!status && (
      <FormGroup>
        <Banner state="error" message={status} />
      </FormGroup>
    )}
    <FastField name="password">
      {({ field, form }: FieldProps<ConfirmUserFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-password" />
            </InputAddon>
            <InputText
              type="password"
              placeholder="Password"
              autoFocus
              addon
              errors={!!form.errors.password && form.touched.password}
              {...field}
            />
          </InputGroup>
          {!!form.errors.password && form.touched.password && <FormError>{form.errors.password}</FormError>}
        </FormGroup>
      )}
    </FastField>
    <FastField name="confirm_password">
      {({ field, form }: FieldProps<ConfirmUserFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-password" />
            </InputAddon>
            <InputText
              type="password"
              placeholder="Confirm password"
              addon
              errors={!!form.errors.confirm_password && form.touched.confirm_password}
              {...field}
            />
          </InputGroup>
          {!!form.errors.confirm_password && form.touched.confirm_password && (
            <FormError>{form.errors.confirm_password}</FormError>
          )}
        </FormGroup>
      )}
    </FastField>
    <div className="mt-3">
      <Button block type="submit" color="primary" disabled={!isValid} loading={isSubmitting}>
        Sign Up
      </Button>
    </div>
  </Form>
);

export const ConfirmUserForm: React.FC<ConfirmUserFormProps> = ({ userId, onSuccess }) => (
  <Formik
    validationSchema={validationSchema}
    initialValues={initialValues}
    onSubmit={async (values, { setSubmitting, setStatus }) => {
      setSubmitting(true);
      const { password } = values;

      try {
        const response = await storePassword(password, userId);

        if (onSuccess) {
          onSuccess(response);
        }
      } catch (err) {
        const errorMessage = get(err, 'response.data.message', 'Unknown error');
        setStatus(`Failed to sign up: ${errorMessage}`);
      } finally {
        setSubmitting(false);
      }
    }}
  >
    {InnerForm}
  </Formik>
);
