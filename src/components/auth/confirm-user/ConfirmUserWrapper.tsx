import * as React from 'react';

import {
  AuthWrapper,
  AuthWrapperWelcome,
  AuthWrapperWelcomeHero,
  AuthWrapperHeroImage,
  AuthWrapperWelcomeTitle,
  AuthWrapperWelcomeSubtitle,
  AuthWrapperLogin,
  AuthWrapperWelcomeFooter,
  AuthWrapperHeroButton,
} from '../AuthWrapper';

import ImgCover from '../../../assets/images/kata-platform-cover.svg';

export const ConfirmUserWrapper: React.FC = ({ children }) => (
  <AuthWrapper className="full-size">
    <AuthWrapperWelcome>
      <AuthWrapperWelcomeHero>
        <AuthWrapperHeroImage src={ImgCover} alt="Kata Platform" className="mb-2" />
        <AuthWrapperWelcomeTitle>Welcome to Kata Platform</AuthWrapperWelcomeTitle>
        <AuthWrapperWelcomeSubtitle>
          Kata | Platform brings together all of the elements to create intelligent chatbot in a single, integrated
          platform. Built with scalability in mind, its engine allows you to design, train, and manage chatbot without
          compromise.
        </AuthWrapperWelcomeSubtitle>
        <AuthWrapperWelcomeFooter>
          <AuthWrapperHeroButton
            href="https://docs.kata.ai/tutorial/bot-studio/"
            target="_blank"
            rel="noopener noreferrer"
          >
            See Tutorial
          </AuthWrapperHeroButton>
          <AuthWrapperHeroButton
            href="https://docs.kata.ai/tutorial/bot-studio/"
            target="_blank"
            rel="noopener noreferrer"
          >
            View Docs
          </AuthWrapperHeroButton>
        </AuthWrapperWelcomeFooter>
      </AuthWrapperWelcomeHero>
    </AuthWrapperWelcome>
    <AuthWrapperLogin>{children}</AuthWrapperLogin>
  </AuthWrapper>
);
