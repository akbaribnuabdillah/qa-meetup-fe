import * as React from 'react';
import { RouteComponentProps } from '@reach/router';
import get from 'lodash-es/get';

import { Circle } from '@kata-kit/loading';
import { Banner } from '@kata-kit/banner';

import { AuthWrapperInner, AuthContainer, AuthWrapperLogo, AuthWrapperFooter, AuthContent } from '../AuthWrapper';
import { ConfirmUserForm } from './ConfirmUserForm';

import Logo from '../../../assets/images/logo-platform.png';
import { checkToken } from '../../../utils/api';
import { CenterText } from '../../ui/CenterText';
import { ConfirmUserSuccess } from './ConfirmUserSuccess';

const WEB_URL = process.env.GATSBY_WEB_URL;

interface RouteParams {
  token: string;
}

interface ConfirmUserFormContainerState {
  isLoading?: boolean;
  isComplete?: boolean;
  userId?: string;
  errors?: string;
}

export class ConfirmUserFormContainer extends React.Component<
  RouteComponentProps<RouteParams>,
  ConfirmUserFormContainerState
> {
  constructor(props: RouteComponentProps<RouteParams>) {
    super(props);

    this.state = {
      isLoading: false,
      isComplete: false,
      errors: undefined,
    };
  }

  public componentDidMount() {
    this.setState({
      isLoading: true,
      errors: undefined,
    });

    if (this.props.token) {
      checkToken(this.props.token)
        .then(res => {
          this.setState({
            isLoading: false,
            userId: res.id,
          });
        })
        .catch(err => {
          const message = get(err, 'response.data.message', 'Unknown error');
          this.setState({
            isLoading: false,
            errors: message,
          });
        });
    }
  }

  public render() {
    return (
      <>
        <AuthWrapperInner>
          <AuthContainer>
            <AuthContent>
              <AuthWrapperLogo className="mb-5">
                <img src={Logo} alt="Kata Platform" />
              </AuthWrapperLogo>
              {this.renderState()}
            </AuthContent>
          </AuthContainer>
        </AuthWrapperInner>
        <AuthWrapperFooter>
          <p>
            Presented by <a href={WEB_URL}>Kata.ai</a>
          </p>
        </AuthWrapperFooter>
      </>
    );
  }

  private renderState() {
    const { isLoading, isComplete, errors, userId } = this.state;

    if (isLoading) {
      return (
        <CenterText className="mb-2">
          <Circle />
        </CenterText>
      );
    }

    if (errors) {
      return <Banner state="error" message={errors} />;
    }

    if (isComplete) {
      return <ConfirmUserSuccess />;
    }

    if (userId) {
      return (
        <ConfirmUserForm
          userId={userId}
          onSuccess={_ => {
            this.setState({
              isComplete: true,
            });
          }}
        />
      );
    }

    return <Banner state="error" message="Unknown Error" />;
  }
}
