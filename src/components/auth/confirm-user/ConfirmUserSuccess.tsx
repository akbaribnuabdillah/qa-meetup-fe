import * as React from 'react';
import styled from 'styled-components';

import { Button } from '@kata-kit/button';

import RegisterComplete from '../../../assets/images/password-success.svg';

const CenterDiv = styled('div')`
  text-align: center;
`;

const GATSBY_PLATFORM_URL = process.env.GATSBY_PLATFORM_URL;

export const ConfirmUserSuccess: React.FC = () => (
  <>
    <CenterDiv className="mt-3">
      <img src={RegisterComplete} className="auth-img" alt="Setup New Password Successfully" />
    </CenterDiv>
    <CenterDiv className="mt-3">
      <h4 className="text-success">Setup New Password Successfully</h4>
      <p className="paragraph">Click the button below to login to the platform.</p>
    </CenterDiv>
    <div className="mt-3">
      <Button
        color="primary"
        type="button"
        block
        onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
          e.preventDefault();
          window.open(`${GATSBY_PLATFORM_URL}`, '_self');
        }}
      >
        Go to Login
      </Button>
    </div>
  </>
);
