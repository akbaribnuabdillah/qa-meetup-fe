import styled from 'styled-components';
import { variables } from '@kata-kit/theme';
import { LandingButton } from '../ui/LandingButton';

export const AuthWrapper = styled('div')`
  display: flex;
  align-items: center;
  min-height: 100vh;
  overflow: auto;
`;

export const AuthWrapperWelcome = styled('div')`
  background: ${variables.colors.kataBlue};
  flex: 0 0 50%;
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 0;
  transition: margin ${variables.transitions.transitionFast} ease;
`;

export const AuthWrapperWelcomeHero = styled('div')`
  text-align: center;
`;

export const AuthWrapperHeroImage = styled('img')`
  height: 100%;
  max-height: 268px;
`;

export const AuthWrapperWelcomeTitle = styled('h1')`
  font-size: 24px;
  font-weight: 500;
  line-height: 1.33;
  color: ${variables.colors.white};
  text-align: center;
  max-width: 80%;
  margin: 24px auto 16px auto;
`;

export const AuthWrapperWelcomeSubtitle = styled('p')`
  margin: 16px auto 24px auto;
  font-size: 16px;
  font-weight: 300;
  line-height: 1.5;
  text-align: center;
  max-width: 80%;
  color: ${variables.colors.white};
`;

export const AuthWrapperLogo = styled('div')`
  text-align: center;

  img {
    height: 100%;
    max-height: 48px;
  }
`;

export const AuthWrapperLogin = styled('div')`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
`;

export const AuthWrapperFooter = styled('div')`
  margin-top: ${variables.spacing.spacingLg}px;
  color: #676b6d;

  p {
    text-align: center;
    margin: 0;
    font-size: ${variables.fontSizes.deka}px;
    line-height: ${variables.lineHeights.deka};
  }
`;

export const AuthWrapperHeroButton = LandingButton.withComponent('a');

export const AuthWrapperWelcomeFooter = styled('div')`
  ${AuthWrapperHeroButton} {
    margin: 0 8px;
  }
`;

export const AuthWrapperInner = styled('div')`
  display: flex;
  flex-direction: column;
`;

export const AuthContainer = styled('div')`
  width: 400px;
  border-radius: 6px;
  background-color: ${variables.colors.white};
  box-shadow: ${variables.layerShadows.layer100Shadow};
`;

export const AuthContent = styled('div')`
  padding: 40px;
`;

export const AuthFooter = styled('div')`
  padding: 24px 40px;
  border-top: 1px solid ${variables.colors.neutral04};

  p {
    text-align: center;
    margin: 0;
    font-weight: 500;
  }
`;
