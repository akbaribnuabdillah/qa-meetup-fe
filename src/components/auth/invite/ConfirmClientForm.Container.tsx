import * as React from 'react';
import { RouteComponentProps } from '@reach/router';
import get from 'lodash-es/get';
import jwt_decode from 'jwt-decode';

import { Circle } from '@kata-kit/loading';
import { Banner } from '@kata-kit/banner';

import { AuthWrapperInner, AuthContainer, AuthWrapperLogo, AuthWrapperFooter, AuthContent } from '../AuthWrapper';
import { ConfirmClientForm } from './ConfirmClientForm';

import Logo from '../../../assets/images/logo-dashboard.svg';
import { checkInviteToken } from '../../../utils/api';
import { CenterText } from '../../ui/CenterText';
import { ConfirmClientSuccess } from './ConfirmClientSuccess';

const WEB_URL = process.env.GATSBY_WEB_URL;

interface RouteParams {
  token: string;
}

interface ConfirmClientFormContainerState {
  isLoading?: boolean;
  isComplete?: boolean;
  userId?: string;
  errors?: string;
}

export class ConfirmClientFormContainer extends React.Component<
  RouteComponentProps<RouteParams>,
  ConfirmClientFormContainerState
> {
  constructor(props: RouteComponentProps<RouteParams>) {
    super(props);

    this.state = {
      isLoading: false,
      isComplete: false,
      errors: undefined,
    };
  }

  public componentDidMount() {
    this.setState({
      isLoading: true,
      errors: undefined,
    });

    if (this.props.token) {
      checkInviteToken(this.props.token)
        .then(res => {
          this.setState({
            isLoading: false,
            userId: res.id,
          });
        })
        .catch(err => {
          const message = get(err, 'response.data.message', 'Unknown error');
          this.setState({
            isLoading: false,
            errors: message,
          });
        });
    }
  }

  public render() {
    return (
      <>
        <AuthWrapperInner>
          <AuthContainer>
            <AuthContent>
              <AuthWrapperLogo className="mb-5">
                <img src={Logo} alt="Kata Platform" />
              </AuthWrapperLogo>
              {this.renderState()}
            </AuthContent>
          </AuthContainer>
        </AuthWrapperInner>
        <AuthWrapperFooter>
          <p>
            Presented by <a href={WEB_URL}>Kata.ai</a>
          </p>
        </AuthWrapperFooter>
      </>
    );
  }

  private renderState() {
    const { isLoading, isComplete, errors, userId } = this.state;
    const { token } = this.props;
    const decodedToken: any = token ? jwt_decode(token) : undefined;

    if (isLoading) {
      return (
        <CenterText className="mb-2">
          <Circle />
        </CenterText>
      );
    }

    if (errors) {
      return <Banner state="error" message={errors} />;
    }

    if (isComplete) {
      return <ConfirmClientSuccess namespace={decodedToken.namespace} />;
    }

    if (userId) {
      return (
        <ConfirmClientForm
          userId={userId}
          email={decodedToken ? decodedToken.email : ''}
          onSuccess={_ => {
            this.setState({
              isComplete: true,
            });
          }}
        />
      );
    }

    return <Banner state="error" message="Unknown Error" />;
  }
}
