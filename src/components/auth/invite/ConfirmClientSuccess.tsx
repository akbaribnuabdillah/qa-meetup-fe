import * as React from 'react';
import { CenterText } from '../../ui/CenterText';

import { Button } from '@kata-kit/button';

import RegisterComplete from '../../../assets/images/password-success.svg';

interface Props {
  namespace: string;
}

const DASHBOARD_URL = process.env.GATSBY_DASHBOARD_URL;

export const ConfirmClientSuccess: React.FC<Props> = namespace => (
  <>
    <CenterText className="mt-3">
      <img src={RegisterComplete} className="auth-img" alt="Setup New Password Successfully" />
    </CenterText>
    <CenterText className="mt-3">
      <h4 className="text-success">Setup New Password Successfully</h4>
      <p className="paragraph">Click the button below to login to the dashboard.</p>
    </CenterText>
    <div className="mt-3">
      <Button
        color="primary"
        type="button"
        block
        onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
          e.preventDefault();
          window.open(`${DASHBOARD_URL}/login/${namespace.namespace}`, '_self');
        }}
      >
        Go to Login
      </Button>
    </div>
  </>
);
