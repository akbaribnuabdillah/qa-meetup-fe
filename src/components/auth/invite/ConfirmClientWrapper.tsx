import * as React from 'react';

import {
  AuthWrapper,
  AuthWrapperWelcome,
  AuthWrapperWelcomeHero,
  AuthWrapperHeroImage,
  AuthWrapperWelcomeTitle,
  AuthWrapperWelcomeSubtitle,
  AuthWrapperLogin,
} from '../../../components/auth/AuthWrapper';

import ImgCover from '../../../assets/images/kata-dashboard-cover.svg';

export const ConfirmClientWrapper: React.FC = ({ children }) => (
  <AuthWrapper className="full-size">
    <AuthWrapperWelcome>
      <AuthWrapperWelcomeHero>
        <AuthWrapperHeroImage src={ImgCover} alt="Kata Dashboard" className="mb-2" />
        <AuthWrapperWelcomeTitle>Welcome to Kata Dashboard</AuthWrapperWelcomeTitle>
        <AuthWrapperWelcomeSubtitle>
          Kata | Dashboard is a content management system for your bot to improve your bot's knowledge base on the fly.
        </AuthWrapperWelcomeSubtitle>
      </AuthWrapperWelcomeHero>
    </AuthWrapperWelcome>
    <AuthWrapperLogin>{children}</AuthWrapperLogin>
  </AuthWrapper>
);
