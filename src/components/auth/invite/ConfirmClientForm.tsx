import * as React from 'react';
import * as Yup from 'yup';
import get from 'lodash-es/get';
import { Formik, FormikProps, Form, FastField, FieldProps } from 'formik';

import { Banner } from '@kata-kit/banner';
import { Button } from '@kata-kit/button';
import { FormGroup, InputGroup, InputAddon, InputText, FormError } from '@kata-kit/form';

import { ConfirmClientFormValues } from '../../../interfaces';
import { CenterText } from '../../ui/CenterText';
import { storeInviteUser } from '../../../utils/api';

const validationSchema = Yup.object({
  username: Yup.string()
    .matches(
      /^[a-zA-Z0-9_]+$/, // allow letters, numbers, and underscores
      'Please enter a valid username. A username should only contain alphanumeric characters, hyphens and underscores (no spaces).'
    )
    .min(5, 'Username must be 5-15 characters long.')
    .max(15, 'Username must be 5-15 characters long.')
    .required('Username is required.'),
  password: Yup.string()
    .min(8, 'Password must be at least 8 characters.')
    .required('Please enter a password.'),
  confirm_password: Yup.string()
    .min(8, 'Password must be at least 8 characters.')
    .oneOf([Yup.ref('password')], `Passwords don't match.`)
    .required('Please enter your password again.'),
});

interface ConfirmClientFormProps {
  userId: string;
  email?: string;
  onSuccess?: (response: ConfirmClientFormValues) => void;
}

const InnerForm = ({ isSubmitting, isValid, status }: FormikProps<ConfirmClientFormValues>) => (
  <Form>
    <CenterText>
      <h4>Create a password</h4>
      <p>Welcome to Kata Dashboard! To get started, please enter an username and password for your account.</p>
    </CenterText>
    {!!status && (
      <FormGroup>
        <Banner state="error" message={status} />
      </FormGroup>
    )}
    <FastField name="username">
      {({ field, form }: FieldProps<ConfirmClientFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-account" />
            </InputAddon>
            <InputText
              type="text"
              placeholder="Username"
              addon
              errors={!!form.errors.username && form.touched.username}
              {...field}
            />
          </InputGroup>
          {!!form.errors.username && form.touched.username && <FormError>{form.errors.username}</FormError>}
        </FormGroup>
      )}
    </FastField>
    <FastField name="email">
      {({ field }: FieldProps<ConfirmClientFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-email" />
            </InputAddon>
            <InputText type="email" placeholder="Email" addon {...field} disabled />
          </InputGroup>
        </FormGroup>
      )}
    </FastField>
    <FastField name="password">
      {({ field, form }: FieldProps<ConfirmClientFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-password" />
            </InputAddon>
            <InputText
              type="password"
              placeholder="Password"
              addon
              errors={!!form.errors.password && form.touched.password}
              {...field}
            />
          </InputGroup>
          {!!form.errors.password && form.touched.password && <FormError>{form.errors.password}</FormError>}
        </FormGroup>
      )}
    </FastField>
    <FastField name="confirm_password">
      {({ field, form }: FieldProps<ConfirmClientFormValues>) => (
        <FormGroup>
          <InputGroup>
            <InputAddon isIcon>
              <i className="icon-password" />
            </InputAddon>
            <InputText
              type="password"
              placeholder="Confirm password"
              addon
              errors={!!form.errors.confirm_password && form.touched.confirm_password}
              {...field}
            />
          </InputGroup>
          {!!form.errors.confirm_password && form.touched.confirm_password && (
            <FormError>{form.errors.confirm_password}</FormError>
          )}
        </FormGroup>
      )}
    </FastField>
    <div className="mt-3">
      <Button block type="submit" color="primary" disabled={!isValid} loading={isSubmitting}>
        Sign Up
      </Button>
    </div>
  </Form>
);

export const ConfirmClientForm: React.FC<ConfirmClientFormProps> = ({ userId, onSuccess, email }) => (
  <Formik
    validationSchema={validationSchema}
    initialValues={{
      username: '',
      password: '',
      confirm_password: '',
      email: `${email}`,
    }}
    onSubmit={async (values, { setSubmitting, setStatus }) => {
      setSubmitting(true);
      const { password, username } = values;

      try {
        const response = await storeInviteUser(password, userId, username);

        if (onSuccess) {
          onSuccess(response);
        }
      } catch (err) {
        const errorMessage = get(err, 'response.data.message', 'Unknown error');
        setStatus(`Failed to sign up: ${errorMessage}`);
      } finally {
        setSubmitting(false);
      }
    }}
  >
    {InnerForm}
  </Formik>
);
