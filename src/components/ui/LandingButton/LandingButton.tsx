import styled from 'styled-components';
import { variables } from '@kata-kit/theme';

export const LandingButton = styled('button')`
  display: inline-block;
  padding: 10px 24px;
  color: ${variables.colors.gray70};
  background-color: #fff;
  border-radius: 20px;
  box-shadow: ${variables.layerShadows.layer200Shadow};
  font-size: 13px;
  font-weight: 700;
  line-height: 1.54;
  letter-spacing: 0.2px;
  text-align: center;
  transition: all ${variables.transitions.transitionFast} ease;

  &:hover {
    color: ${variables.colors.kataBlue};
    text-decoration: none;
    box-shadow: 0 6px 8px 0 rgba(0, 0, 0, 0.25);
    transform: translateY(-1px);
  }

  &:active:not(:disabled) {
    background: inherit, rgba(0, 0, 0, 0.1);
    box-shadow: ${variables.layerShadows.layer200Shadow};
    transform: translateY(0);
  }
`;
