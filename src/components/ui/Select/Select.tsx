import styled, { css } from 'styled-components';
import { variables } from '@kata-kit/theme';

export interface SelectProps extends React.InputHTMLAttributes<HTMLSelectElement> {
  /** Alternate form state for input with errors. */
  errors?: boolean;
  /** True if this text input has an addon style */
  addon?: boolean;
}

const WithAddonStyles = css`
  position: relative;
  flex: 1 1 auto;
  width: 1%;
  margin-bottom: 0;
  border-top-left-radius: 0 !important;
  border-bottom-left-radius: 0 !important;
`;

export const Select = styled('select')`
  display: block;
  width: 100%;
  height: 40px;
  padding: 8px 24px 8px 16px;
  box-sizing: border-box;
  border-radius: 6px !important;
  background: ${variables.colors.white}
    url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3E%3Cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3E%3C/svg%3E")
    no-repeat right 0.75rem center;
  background-size: 8px 10px;
  border: 1px solid ${variables.colors.gray30};
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0);
  transition: all 0.3s ease;
  outline: none;

  :not([multiple]) {
    -webkit-appearance: none;
    border-radius: 0;
  }

  &::placeholder {
    color: ${variables.colors.gray50};
  }

  &:disabled,
  &.disabled {
    background-color: ${variables.colors.gray10};
    color: ${variables.colors.gray50};
  }

  &:not(:disabled):not(.disabled) {
    &:hover {
      background-color: ${variables.colors.gray10};
      border-color: ${variables.colors.kataBlue};
    }
    &:active,
    &:focus {
      background-color: ${variables.colors.white};
      border: 1px solid ${variables.colors.kataBlue};
      box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.25), 0 0 0 4px ${variables.colors.softKataBlue};
    }
  }

  ${(props: SelectProps) => props.addon && WithAddonStyles};
`;
