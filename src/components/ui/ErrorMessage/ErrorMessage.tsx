import React from 'react';
import styled from 'styled-components';
import { variables } from '@kata-kit/theme';

interface LayoutRootProps {
  image?: string;
  title?: string;
}

export const ErrorMessage: React.SFC<LayoutRootProps> = ({ image, title, children }) => (
  <Root>
    {image && <Image src={image} />}
    <Inner>
      {title && <Title>{title}</Title>}
      <Message>{children}</Message>
    </Inner>
  </Root>
);

const Root = styled('div')`
  margin: 0;
  padding: 24px;
`;

const Inner = styled('div')`
  max-width: 480px;
`;

const Image = styled('img')`
  margin-bottom: 40px;
  width: 480px;
`;

const Title = styled('h1')`
  margin-top: 0;
  margin-bottom: 16px;
  font-size: 32px;
  font-weight: 300;
  color: ${variables.colors.kata02};
`;

const Message = styled('p')`
  margin: 0;
  font-size: 16px;
  line-height: 1.5;
`;
