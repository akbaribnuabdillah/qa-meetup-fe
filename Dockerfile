#first stage
FROM node:lts AS build-env

RUN mkdir -p /akbar
WORKDIR /akbar

ADD . /akbar
RUN ls -alh
RUN yarn install
RUN yarn build

#second stage
FROM gatsbyjs/gatsby:latest
LABEL maintainer="kata.ai"

ENV APPDIR /code
RUN mkdir -p ${APPDIR}
WORKDIR ${APPDIR}

COPY --from=build-env /code/docker/nginx.conf /etc/nginx/server.conf
COPY --from=build-env /code/public/ /code/pub
